이 섹션에서는 해시 테이블 자료 구조를 나타내는 사용자 정의 클래스를 만든다. Python 딕셔너리로 뒷바침되지 않으므로 해시 테이블을 처음부터 구축함으로써 지금까지 배운 내용을 연습할 수 있다. 동시에 내장 딕셔너리의 가장 중요한 기능을 모방하여 구현을 모델링할 수 있다.

> **Note:** 이 장에서 해시 테이블을 구현하는 것은 이 자료 구조가 해결하고자 하는 문제에 대해 알려주는 연습이자 교육 도구일 뿐이라는 점을 잊지 말자. 이전에 우리가 정의한 해시 함수를 코딩한 것처럼, 순수 Python 해시 테이블 구현은 실제 응용 프로그램에서 실용적으로 사용되지 않는다.

다음은 현재 구현 중인 해시 테이블에 대한 높은 수준의 요구 사항 목록이다. 이 장을 마치면 해시 테이블은 다음과 같은 **핵심 기능**을 갖는다. 다음과 같은 기능을 제공합니다.

- 빈 해시 테이블 생성
- 해시 테이블에 key-value 쌍 삽입
- 해시 테이블에서 key-value 쌍 삭제
- 해시 테이블에서 키별 값 찾기
- 기존 키와 연결된 값 업데이트
- 해시 테이블에 지정된 키가 있는지 확인

이러한 기능 외에도 몇 가지 **필수적이지는 않지만** 유용한 기능을 구현할 수 있다. 구체적으로 다음을 수행할 수 있어야 한다.

- Python 딕셔너리에서 해시 테이블 만들기
- 기존 해시 테이블의 얕은 복사본 만들기
- 해당 키를 찾을 수 없는 경우 기본값 반환
- 해시 테이블에 저장된 key-value 쌍 수 보고
- 키, 값 및 key-value 쌍 반환
- 해시 테이블을 iterable 속성 설정
- 동등성(equality) 테스트 연산자로 해시 테이블을 비교
- 해시 테이블을 텍스트로 표시

이러한 기능을 구현하며 해시 테이블에 점점 더 많은 기능을 추가하여 **테스트 주도 개발(test-driven development)**을 적극적으로 수행할 수 있다. 프로토타입에는 기본 사항만 포함될 것이다. 이 튜토리얼의 뒷부분에서 고급 기능을 처리하는 방법에 대해 알아본다. 이 장에서는 특히 다음과 같은 방법에 대해서는 다루지 않는다.

- 해시 코드 충돌 해결
- 삽입 순서 유지
- 동적으로 해시 테이블 크기를 조정
- 부하율 계산

만약 여러분이 막혔거나 중간 리팩터링 단계를 건너뛰고 싶다면, 보조 재료를 **제어 체크 포인트(control checkpoint)**로 얼마든지 사용할 수 있다. 각 하위 섹션은 전체 구현 단계와 시작할 수 있는 해당 테스트로 끝낸다. 다음 링크를 클릭하면 전체 소스 코드와 이 튜토리얼서 사용하는 중간 단계가 포함된 지원 자료를 다운로드할 수 있다.

> **Source Code:** Python에서 해시 테이블을 구축하는 데 사용할 **소스 코드를 다운로드하려면 [여기](https://realpython.com/bonus/python-hash-table-source-code/)를 클릭하시오**.

## <a name='crash-courese-tdd'>테스트 주도 개발 집중 과정</a>
이제 해시 함수의 높은 수준의 특성과 그 목적을 알았으므로 [테스트 중심 개발](https://realpython.com/courses/test-driven-development-pytest/) 접근법을 사용하여 해시 테이블을 작성할 수 있다. 이전에 테스트 주도 개발 기술을 경험해 본 적이 없는 경우 세 단계로 요약된 사이클을 반복한다.

1. 🟥 Red: 단일 [테스트 케이스(test case)](https://en.wikipedia.org/wiki/Test_case)로 원하는 [단위 테스트(unit testing)](https://en.wikipedia.org/wiki/Unit_testing) 프레임워크를 사용하여 자동화한다. 이 시점에서 여러분의 테스트는 실패하겠지만, 괜찮다. 테스트 수행자는 일반적으로 red로 실패한 테스트를 나타내며, 따라서 이 사이클 단계의 이름으로 불린다.
2. 🟩 Green: 테스트를 통과할 수 있도록 최소 경우를 구현한다. 그 이상은 사용할 수 없다! 이는 더 높은 [코드 커버리지(code coverage)](https://en.wikipedia.org/wiki/Code_coverage)를 보장하고 중복 코드를 작성하지 않아도 된다. 테스트 보고서에서 나중에 만족스러운 green으로 표시된다.
3. ♻ Refactor: 선택적으로 모든 테스트 케이스를 통과하는 한에서 작동을 변경하지 않고 코드를 수정한다. 중복을 제거하고 코드의 가독성을 향상시키는 기회로 사용할 수 있다.

Python은 기본적으로 [unittest](https://docs.python.org/3/library/unittest.html) 패키지가 함께 제공되지만, 서드파티 [pytest](https://realpython.com/pytest-python-testing/) 라이브러리는 거의 틀림없이 더 낮은 학습 곡선을 가지고 있기 때문에 이 튜토리얼에서 대신 사용할 수 있다. 이제 가상 환경에 pytest를 설치하시오.

```
(venv) $ python -m pip install pytest
```

각 구현 단계에서 여러 제어 체크포인트를 기준으로 확인할 수 있다. 그런 다음 `test_hashtable.py`라는 이름의 파일을 만들고 그 안에 더미 테스트 함수를 정의하여 `pytest`가 이를 픽업하는지 여부를 확인한다.

```python
# test_hashtable.py

def test_should_always_pass():
    assert 2 + 2 == 22, "This is just a dummy test"
```

이 프레임워크는 내장된 [assert 문](https://realpython.com/python-assert-statement/)을 활용하여 테스트를 실행하고 결과를 보고한다. 즉, 꼭 필요할 때까지 특정 API를 가져오지 않고 일반 Python 구문을 사용할 수 있다. 또한 이름이 `test` 접두사로 시작하는 한 테스트 파일ㅇㅡㄹ 탐지하고 테스트 기능을 수행한다.

`assert` 문은 부울식을 인수로 사용한 다음 선택적 오류 메시지를 출력한다. 조건이 `True`로 평가되면 어떤 assertion이 없었던 것처럼 아무 일도 일어나지 않는다. 그렇지 않으면 Python은 `AssertionError`를 발생시키고 [표준 오류 스트림(stderr)](https://en.wikipedia.org/wiki/Standard_streams#Standard_error_(stderr))에 메시지를 디스플레이한다. 한편, `pytest`는 assertion 오류를 가로채어 그 상황에 대한 보고서를 작성한다.

이제 터미널에서 작업 디렉터리로 해당 테스트 파일을 저장한 위치로 변경한 다음 인수 없이 pytest 명령을 실행하여 보자. 출력은 다음과 유사할 것이다.

```python
(venv) $ pytest
=========================== test session starts ===========================
platform linux -- Python 3.10.0, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
rootdir: /home/realpython/hashtable
collected 1 item

test_hashtable.py F                                                 [100%]

================================ FAILURES =================================
_________________________ test_should_always_pass _________________________

    def test_should_always_pass():
>       assert 2 + 2 == 22, "This is just a dummy test"
E       AssertionError: This is just a dummy test
E       assert (2 + 2) == 22

test_hashtable.py:4: AssertionError
========================= short test summary info =========================
FAILED test_hashtable.py::test_should_always_pass - AssertionError: This...
============================ 1 failed in 0.03s ============================
```

오,  테스트에 문제가 생겼어요!. 근본 원인을 찾으려면 명령에 `-v` 플래그를 추가하여 자세한 `pytest` 출력을 얻을 수 있다. 이제 문제가 있는 위치를 정확히 파악할 수 있다.

```
   def test_should_always_pass():
>       assert 2 + 2 == 22, "This is just a dummy test"
E       AssertionError: This is just a dummy test
E       assert (2 + 2) == 22

test_hashable.py:4: AssertionError
```

출력에는 실패한 assertion에 대한 **실제 값**과 **기대 값**이 표시되었다. 이 경우 2에 2를 더하면 22가 아니라 4가 된다. 예상 값을 수정하여 코드를 수정할 수 있다.

```python
# test_hashtable.py

def test_should_always_pass():
    assert 2 + 2 == 4, "This is just a dummy test"
```

`pytest`를 다시 실행하면 더 이상 테스트 실패가 없어야 한다.

```bash
$ pytest
=================================== test session starts ====================================
platform linux -- Python 3.11.0, pytest-7.2.0, pluggy-1.0.0
rootdir: /home/yoonjoon.lee/Execises/PyTestEx
collected 1 item                                                                           

test_hashable.py .                                                                   [100%]

==================================== 1 passed in 0.00s =====================================
```

끝이다! 이제 해시 테이블 구현을 위한 테스트 사례를 자동화하는 데 필수적인 단계를 배웠다. 물론 [PyCharm](https://realpython.com/pycharm-guide/)과 같은 IDE나 테스트 프레임워크와 통합되는 [VS Code](https://realpython.com/advanced-visual-studio-code-python/)와 같은 편집기를 사용할 수도 있다. 다음으로, 여러분은 이 새로운 지식을 실행에 옮길 것이다.

## <a name='define-custom-hash-table'>사용자 정의 해시 테이블 정의</a>
앞서 설명한 **red-green-refactor** 싸이클을 따르는 것을 기억하시오. 따라서 첫 번째 테스트 케이스를 작성하는 것부터 시작해야 한다. 예를 들어 `hashtable` 모듈에서 import한 가상 `HashTable` 클래스를 호출하여 새 해시 테이블을 인스턴스화할 수 있어야 한다. 이 호출은 non-empty 객체를 반환해야 한다.

```python
# test_hashtable.py

from hashtable import HashTable

def test_should_create_hashtable():
    assert HashTable() is not None
```

이 때 파일 맨 위에 있는 import 문 때문에 테스트 실행이 거부된다. 결국 여러분은 **Red 단계**에 있는 것이다. red색 단계에서는 새 기능을 추가할 수 있는 유일한 시간이므로 계속해서 `hashtable.py`이라는 다른 모듈을 만들고 여기에 `HashTable` 클래스 정의를 추가하시오.

```python
# hashtable.py

class HashTable:
    pass
```

골격만 있는 클래스 자리 표시자이지만, 테스트에 합격하기에 충분할 것다. 참고로 코드 편집기를 사용하는 경우 보기를 열로 분할하여 테스트 대상 코드와 해당 테스트를 나란히 표시할 수 있다.

![editor_view](./images/fig-3-1.png)

`pytest` 수행이 성공되면 아직 리팩터링할 수 있는 것이 거의 없기 때문에 다른 테스트 케이스를 생각할 수 있다. 예를 들어, 기본 해시 테이블은 일련의 값을 포함해야 한다. 이 초기 단계에서는 해시 테이블의 생성 시 시퀀스의 **크기가 고정**되어 있다고 가정한다. 그에 따라 기존 테스트 케이스를 수정한다.

```python
# test_hashtable.py

from hashtable import HashTable

def test_should_create_hashtable():
    assert HashTable(size=100) is not None
```

[키워드 인수](https://docs.python.org/3/glossary.html#term-keyword-argument)를 사용하여 크기를 지정한다. 그러나 `HashTable` 클래스에 새 코드를 추가하기 전에 테스트를 다시 실행하여 red 단계에 다시 들어갔는지 확인하자. 실패하는 시험을 목격하는 것은 매우 귀중한 경험일 수 있다. 나중에 코드를 구현하면 특정 테스트 그룹을 충족하는지 또는 영향을 받지 않는지 알 수 있다. 그렇지 않으면, 여러분의 테스트가 여러분이 생각했던 것과 다른 것을 확인함으로써 여러분을 혼란에 빠뜨릴 수 있다!

red 단계에 있는 것을 확인한 후 `HashTable` 클래스에서 `.__init__()` 메서드를 예상 시그네이처로 선언하지만 본문을 구현하지는 않았다.

```python
# hashtable.py

class HashTable:
    def __init__(self, size):
        pass
```

다시 green 단계로 돌아왔다. 이번에는 리팩터링을 해봅시다. 예를 들어, 더 자세히 설명한다면, `size` 인수의 이름을 `capacity`로 변경할 수 있다. 먼저 테스트 케이스를 변경한 다음 `pytest`를 실행하고 마지막 단계로 항상 테스트 대상 코드를 업데이트하는 것을 잊지 말자.

```python
# hashtable.py

class HashTable:
    def __init__(self, capacity):
        pass
```

여러분이 알 수 있듯이, red-green-refactor 주기는 짧은 단계로 구성되며, 종종 몇 초 이하 동안 지속된다. 그렇다면 테스트 케이스를 추가하여 계속 진행해 보는 것은 어떨까? 여러분의 자료 구조가 Python의 내장된 `len()` 함수를 사용하여 해시 테이블의 용량을 다시 보고할 수 있다면 좋을 것이다. 다른 테스트 케이스를 추가하고 비참하게 실패하는 방법을 관찰해 보자.

```python
# test_hashtable.py

from hashtable import HashTable

def test_should_create_hashtable():
    assert HashTable(capacity=100) is not None

def test_should_report_capacity():
    assert len(HashTable(capacity=100)) == 100
```

`len()`을 바르게 처리하려면 클래스에서 [special 메서드](https://docs.python.org/3/glossary.html#term-special-method) `.__len__()`를 구현하고 클래스 초기화를 통해 제공된 용량을 기억하도록 해야 한다.

```python
# hashtable.py

class HashTable:
    def __init__(self, capacity):
        self.capacity = capacity

    def __len__(self):
        return self.capacity
```

여러분은 TDD가 여러분을 올바른 방향으로 인도하지 않는다고 생각할지도 모른다. 여러분은 해시 테이블 구현을 그렇게 생각하지 않았을 것이다. 처음에 시작한 값의 순서는 어디에 있을까? 불행하게도, 도중에 걸음마 단계를 밟고 많은 코스를 수정하는 것은 시험 중심의 개발에 대해 많은 비판을 받는 것이다. 따라서 많은 실험이 포함된 프로젝트에는 적합하지 않을 수 있다.

반면에, 해시 테이블과 같은 잘 알려진 자료 구조를 구현하는 것은 이 소프트웨어 개발 방법론을 완벽하게 적용할 수 있다. 테스트 케이스로 쉽게 분류할 수 있는 명확한 기대치가 있기 때문이다. 곧, 여러분은 다음 단계를 밟는 것이 구현에 약간의 변화를 가져올 것이라는 것을 스스로 알게 될 것아다. 그러나 코드 자체를 완벽하게 만드는 것이 테스트 케이스를 통과시키는 것보다 덜 중요하므로 걱정하지 말자.

테스트 케이스를 통해 더 많은 제약 조건을 추가함에 따라 구현을 재고해야 하는 경우가 많다. 예를 들어, 저장된 값이 빈 슬롯으로 새로운 해시 테이블을 시작해야 한다.

```python
# test_hashtable.py

# ...

def test_should_create_empty_value_slots():
    assert HashTable(capacity=3).values == [None, None, None]
```

즉, 새로운 해시 테이블은 요청된 길이를 갖으며 `None` 요소로 채워진 `.values` 속성을 표시해야 한다. 그런데 그런 함수 이름이 테스트 보고서에 표시되기 때문에 상세한 이름을 사용하는 것이 일반적이다. 테스트가 더 읽기 쉽고 설명적일수록 수정해야 할 부분을 더 빨리 파악할 수 있다.

> **Notes:** 경험에 비추어 볼 때, 테스트 케이스는 가능한 한 독립적이고 아토믹해야 한다. 즉, 일반적으로 함수당 하나의 assertion만 사용한다. 그럼에도 불구하고 테스트 시나리오에는 약간의 설정 또는 해체가 필요할 수 있다. 또한 몇 가지 단계를 포함할 수도 있다. 이러한 경우, 여러분의 함수를 소위 [given-when-then](https://en.wikipedia.org/wiki/Given-When-Then)이라는 관례를 중심으로 구성하도록 한다.

```python
def test_should_create_empty_value_slots():
    # Given
    expected_values = [None, None, None]
    hash_table = HashTable(capacity=3)

    # When
    actual_values = hash_table.values

    # Then
    assert actual_values == expected_values
```

> `given` 부분은 테스트 케이스의 초기 상태와 전제 조건을 기술하며, `when` 테스트 중인 코드의 작동을 나타내고, `then`은 결과를 확인하는 역할을 한다.

다음은 기존 테스트를 만족시키는 여러 가지 방법 중 하나이다.

```python
# hashtable.py

class HashTable:
    def __init__(self, capacity):
        self.values = capacity * [None]

    def __len__(self):
        return len(self.values)
```

`.capacity` 속성을 `None`으로만 구성하여 요청된 길이의 리스트로 바꾼다. 숫자에 리스트을 곱하여 지정된 값을 해당 목록에 빠르게 채울 수 있다. 그 외에는 special 메서드 `.__len__()`을 업데이트하여 모든 테스트가 통과되도록 한다.

> **Notes:** Python 딕셔너리는 내부 용량 대신 저장된 실제 key-value 쌍 수에 해당하는 길이를 갖는다. 여러분은 곧 비슷한 결과를 얻을 수 있을 것이다.

TDD에 대해 공부하였으니 이제 더 깊이 들어가 해시 테이블에 나머지 기능을 추가할 차례이다.

## <a name='insert-key-value-pair'>key-value 쌍 삽입</a>
이제 해시 테이블을 생성할 수 있으므로 몇 가지 저장 기능을 제공해야 한다. 기존 해시 테이블은 하나의 데이터 타입만 저장할 수 있는 배열로 구성하였다. 이 때문에 자바와 같은 많은 언어에서 해시 테이블 구현할 때 키와 값의 타입을 미리 선언해야 했다.

```java
Map<String, Integer> phonesByNames = new HashMap<>();
```

예를 들어, 위의 해시 테이블은 문자열을 정수로 매핑한다. 그러나 배열은 Python에서 기본 데이터 터입이 아니기 때문에 대신 리스트를 계속 사용하게 된다. 그 결과로, 여러분의 해시 테이블은 Python의 `dict`처럼 키와 값 모두에 대하여 임의의 모든 데이터 타입을 받아들일 수 있다.

> **Note:** Python은 효율적인 [배열](https://docs.python.org/3/library/array.html) 컬렉션을 가지고 있지만 숫자 값에만 사용된다. 이진 데이터를 처리하는 데 편리할 수 있다.

이제 익숙한 대괄호 구문을 사용하여 key-value 쌍을 해시 테이블에 삽입하기 위한 다른 테스트 케이스를 추가한다.

```python
# test_hashtable.py

# ...

def test_should_insert_key_value_pairs():
    hash_table = HashTable(capacity=100)

    hash_table["hola"] = "hello"
    hash_table[98.6] = 37
    hash_table[False] = True

    assert "hello" in hash_table.values
    assert 37 in hash_table.values
    assert True in hash_table.values
```

먼저 빈 슬롯이 100개인 해시 테이블을 만든 다음 문자열, 부동 소수점 실수와 부울을 포함한 다양한 유형의 키와 값의 세 쌍을 테이블에 넣는다. 마지막으로 해시 테이블에 기대 값이 순서에 상관없이 포함되어 있다고 assert 한다. 해시 테이블은 값만 기억하고 현재 연결된 키는 기억하지 않는다!

이 테스트를 만족시킬 수 있는 가장 단순하고 약간 직관적인 구현은 다음과 같다.

```python
# hashtable.py

class HashTable:
    def __init__(self, capacity):
        self.values = capacity * [None]

    def __len__(self):
        return len(self.values)

    def __setitem__(self, key, value):
        self.values.append(value)
```

키를 완전히 무시하고 값을 리스트의 오른쪽 끝에 추가하여 키의 길이를 늘린다. 여러분은 즉시 다른 테스트 케이스를 생각할 수 있다. 해시 테이블에 원소를 삽입하면 크기가 커져서는 안된다. 마찬가지로 요소를 제거하면 해시 테이블이 줄어들지 않지만 key-value 쌍을 제거할 수 있는 기능이 아직 없지만 이를 염두에 두어야 한다.

> **Note:** 또한 자리만 잡도록 테스트를 작성하고 나중까지 무조건 건너뛰도록 pytest에 지시할 수 있다.
>
```python
import pytest

@pytest.mark.skip
def test_should_not_shrink_when_removing_elements():
    pass
```
>
> pytest에서 제공하는 [decorators](https://sdldocs.gitlab.io/pythonnecpsystem/primeronpythondecorators) 중 하나를 활용한다.


실제 환경에서는 이러한 연산 테스트를 설명하는 이름을 사용하여 별도의 테스트 케이스를 만들기도 한다. 그러나 지금은 튜토리얼이기 때문에, 우리는 간단히 기존 함수에 새로운 assertion을 추가할 것이다.

```python
# test_hashtable.py

# ...

def test_should_insert_key_value_pairs():
    hash_table = HashTable(capacity=100)

    hash_table["hola"] = "hello"
    hash_table[98.6] = 37
    hash_table[False] = True

    assert "hello" in hash_table.values
    assert 37 in hash_table.values
    assert True in hash_table.values

    assert len(hash_table) == 100
```

지금은 red 단계이므로 용량을 항상 유지할 수 있는 특별한 방법을 다시 작성하여야 한다.

```python
# hashtable.py

class HashTable:
    def __init__(self, capacity):
        self.values = capacity * [None]

    def __len__(self):
        return len(self.values)

    def __setitem__(self, key, value):
        index = hash(key) % len(self)
        self.values[index] = value
```

임의 키에 행당하는 숫자 해시 값으로 변환하고 모듈로 연산자를 사용하여 사용 가능한 주소 공간 내에서 인덱스를 제한할 수 있다. 좋아요! 테스트 보고서가 다시 green으로 변한다.

> **Note:** 위의 코드는 이미 언급했던 바와 같이 랜덤화를 지닌 Python의 내장 `hash()` 함수에 의존한다. 따라서 두 키가 우연히 동일한 해시 코드를 생성하는 드문 경우에 검정이 실패할 수 있다. 해시 코드 충돌은 나중에 처리하기 때문에 그 사이에 pytest를 실행할 때 해시 랜덤화를 비활성화하거나 예측 가능한 시드를 사용할 수 있다.
>
```bash
$ PYTHONHASHSEED=128 pytest
```
>
> 샘플 데이터에서 충돌을 일으키지 않는 해시 시드를 선택해야 한다. 하나를 찾는 것은 약간의 시행착오를 수반할 수 있다. 우리의 경우 `128`은 잘 작동하는 것처럼 보였다.

하지만 혹시 에지 케이스를 생각할 수 있나요? 해시 테이블에 값 없음을 삽입하는 것은 어떨까? 해시 테이블에서 공백을 나타내기 위해 선택한 합법적인 값과 지정된 센티넬 값 사이에 충돌이 발생할 수 있다. 여러분은 그것을 피하고 싶을 것이다.

항상 그렇듯이 바라는 연산을 표현하기 위한 테스트 케이스를 작성하는 것으로 시작한다.

```python
# test_hashtable.py

# ...

def test_should_not_contain_none_value_when_created():
    assert None not in HashTable(capacity=100).values
```

이 문제를 해결하는 한 가지 방법은 `__init__()` 메서드의 `None`을 사용자가 삽입할 가능성이 낮은 다른 고유 값으로 대체하는 것이다. 예를 들어, 해시 테이블의 공백을 나타내는 완전히 새로운 [객체](https://docs.python.org/3/library/functions.html#object)를 만들어 특별한 상수를 정의할 수 있다.

```python
# hashtable.py

BLANK = object()

class HashTable:
    def __init__(self, capacity):
        self.values = capacity * [BLANK]

    # ...
```

슬롯이 비어 있다는 것을 표시하려면 빈 인스턴스 하나만 있으면 된다. 당연히 green 단계로 돌아가려면 이전 테스트 사례를 업데이트해야 한다.

```python
from hashtable import HashTable, BLANK

# ...

def test_should_create_empty_value_slots():
    assert HashTable(capacity=3).values == [BLANK, BLANK, BLANK]

# ...
```

그런 다음 `None` 값 삽입을 처리하기 위한 [happy 경로](https://en.wikipedia.org/wiki/Happy_path)를 연습하는 긍정적인 테스트 케이스를 작성한다.

```python
def test_should_insert_none_value():
    hash_table = HashTable(capacity=100)
    hash_table["key"] = None
    assert None in hash_table.values
```

일부 임의 키와 연결된 `insertNone`으로 슬롯이 100개인 빈 해시 테이블을 생성한다. 지금까지 단계를 잘 따라왔다면 잘 작동할 것이다. 그렇지 않다면, 오류 메시지를 살펴보세오. 오류 메시지에는 종종 무엇이 잘못되었는 지에 대한 단서를 갖고 있기 때문이다. 또는 [링크](https://realpython.com/bonus/python-hash-table-source-code/)에서 다운로드할 수 있는 샘플 코드를 확인하세요.

다음 섹션에서는 연결된 키를 기준으로 값을 검색하는 기능을 추가한다.

## <a name='find-value-by-key'>키로 값 검색</a>
해시 테이블에서 값을 검색하려면 할당 문을 사용하지 않고 이전과 동일한 대괄호 구문을 활용해야 한다. 샘플 해시 테이블도 필요할 것이다. [테스트 수트(test suit)](https://en.wikipedia.org/wiki/Test_suite)의 개별 테스트 케이스에서 동일한 설정 코드가 중복되지 않도록 하려면 pytest에 의해 노출된 [테스트 픽스처(fixture)](https://en.wikipedia.org/wiki/Test_fixture)로 코드를 wrap할 수 있다.

```python
# test_hashtable.py

import pytest

# ...

@pytest.fixture
def hash_table():
    sample_data = HashTable(capacity=100)
    sample_data["hola"] = "hello"
    sample_data[98.6] = 37
    sample_data[False] = True
    return sample_data

def test_should_find_value_by_key(hash_table):
    assert hash_table["hola"] == "hello"
    assert hash_table[98.6] == 37
    assert hash_table[False] is True
```

테스트 픽스처는 `@pytest.fixture` 데코레이터로 annotate된 함수이다. 주어진 키와 값으로 채워진 해시 테이블과 같은 테스트 케이스에 대한 샘플 데이터를 반환한다. pytest는 자동으로 해당 함수를 호출하고 픽스처와 동일한 이름의 인수를 선언하는 테스트 함수에 결과를 삽입한다. 이 경우 테스트 함수는 픽스처 이름에 해당하는 `hash_table` 인수를 기대한다.

키를 사용하여 값을 찾을 수 있도록 `HashTable` 클래스에서 `.__getitem__()`이라는 다른 특별한 메서드를 통해 요소 조회를 구현할 수 있다.

```python
# hashtable.py

BLANK = object()

class HashTable:
    def __init__(self, capacity):
        self.values = capacity * [BLANK]

    def __len__(self):
        return len(self.values)

    def __setitem__(self, key, value):
        index = hash(key) % len(self)
        self.values[index] = value

    def __getitem__(self, key):
        index = hash(key) % len(self)
        return self.values[index]
```

제공된 키의 해시 코드를 기반으로 요소의 인덱스를 계산하고. 해당 인덱스에 해당하는 모든 것을 반환한다. 하지만 잃어버린 키는? 현재 지정된 키가 이전에 사용되지 않은 경우 빈 인스턴스를 반환할 수 있다. 그 결과는 그다지 유용하지 않을 것이다. 이러한 경우 Python `dict`가 작동하는 방식을 따르려면 대신 `KeyError` 예외를 발생시켜야 한다.

```python
# test_hashtable.py

# ...

def test_should_raise_error_on_missing_key():
    hash_table = HashTable(capacity=100)
    with pytest.raises(KeyError) as exception_info:
        hash_table["missing_key"]
    assert exception_info.value.args[0] == "missing_key"
```

빈 해시 테이블을 생성하고 누락된 키를 통해 해당 값 중 하나에 액세스하려고 한다. `pytest` 프레임워크는 예외를 테스트하기 위한 특수 구조를 포함한다. 위에서 `pytest.raise` 컨텍스트 관리자를 사용하여 다음 코드 블록 내에서 특정 종류의 예외를 예상합니다. 이 경우를 처리하려면 액세서(accessor) 메서드에 조건문을 추가해야 한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __getitem__(self, key):
        index = hash(key) % len(self)
        value = self.values[index]
        if value is BLANK:
            raise KeyError(key)
        return value
```

지정된 인덱스의 값이 비어 있으면 예외가 발생한다. 동치성(equality) 테스트 연산자(`==`) 대신 `is` 연산자를 사용하여 [값이 아닌 동일성](https://realpython.com/python-is-identity-vs-equality/)을 비교한다. 사용자 정의 클래스에서 동일성 테스트의 기본 구현은 인스턴스의 ID를 비교하는 것이지만, 대부분의 내장 데이터 타입은 두 연산자를 구별하고 서로 다르게 구현한다.

이제 지정된 키가 해시 테이블에 연결된 값을 가지고 있는지 여부를 확인할 수 있으므로 `in` 연산자를 구현하여 Python `dict`를 모방할 수 있다. TDD 원칙을 준수하기 위해 이러한 테스트 케이스를 개별적으로 작성하고 다루어야 한다.

```python
# test_hashtable.py

# ...

def test_should_find_key(hash_table):
    assert "hola" in hash_table

def test_should_not_find_key(hash_table):
    assert "missing_key" not in hash_table
```

두 테스트 케이스 모두 이전에 준비한 테스트 픽스처를 활용하여 특별한 메소드 `.__contains__()`을 확인한다. 이 메소드는 다음과 같이 구현할 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __contains__(self, key):
        try:
            self[key]
        except KeyError:
            return False
        else:
            return True
```

지정된 키를 액세스할 때 `KeyError`가 발생하면 해당 예외를 가로채어 `False`를 반환하여 없는 키임을 나타낸다. 그렇지 않으면 `try ... except` 블럭을 `else` 절과 결합하고 `True`를 반환한다. [예외 처리](https://realpython.com/python-exceptions/)는 훌륭하지만 때로는 불편할 수 있으므로 `dict.get()`을 사용하여 선택적 **기본값(default value)**을 지정할 수 있다. 사용자 정의 해시 테이블에서 동일한 동작을 복제할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_get_value(hash_table):
    assert hash_table.get("hola") == "hello"

def test_should_get_none_when_missing_key(hash_table):
    assert hash_table.get("missing_key") is None

def test_should_get_default_value_when_missing_key(hash_table):
    assert hash_table.get("missing_key", "default") == "default"

def test_should_get_value_with_default(hash_table):
    assert hash_table.get("hola", "default") == "hello"
```

`.get()` 코드는 방금 구현한 특별한 메서드와 유사하다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default
```

제공된 키에 해당하는 값을 반환한다. 예외의 경우 기본값(선택적 인수)을 반환한다. 사용자가 지정하지 않은 상태로 두면 없음과 같다.

완전성을 위해 다음 섹션에서 해시 테이블에서 key-value 쌍을 삭제하는 기능을 추가한다.

## <a name='delete-key-value-pair'>key-value 쌍 삭제</a>
Python 딕셔너리에서 내장된 `del` 키워드를 사용하여 이전에 삽입된 key-value 쌍을 삭제할 수 있다. 이 키워드는 키와 값에 대한 정보를 모두 제거한다. 이것이 해시 테이블에서 작동하는 방법은 다음과 같다.

```python
# test_hashtable.py

# ...

def test_should_delete_key_value_pair(hash_table):
    assert "hola" in hash_table
    assert "hello" in hash_table.values

    del hash_table["hola"]

    assert "hola" not in hash_table
    assert "hello" not in hash_table.values
```

먼저 샘플 해시 테이블에 원하는 키와 값이 있는지 확인한다. 그런 다음 키만 표시하여 둘 다 삭제하고, 기대에 반대인 assertion을 반복한다. 이 테스트를 다음과 같이 통과시킬 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        index = hash(key) % len(self)
        del self.values[index]
```

키와 연결된 인덱스를 계산하고 리스트에서 해당 값을 무조건 제거한다. 그러나 해시 테이블에서 요소를 삭제할 때 해시 테이블이 축소되지 않아야 한다는 이전의 메모를 기억하자. 따라서 다음 두 assertion을 추가한다.

```python
# test_hashtable.py

# ...

def test_should_delete_key_value_pair(hash_table):
    assert "hola" in hash_table
    assert "hello" in hash_table.values
    assert len(hash_table) == 100

    del hash_table["hola"]

    assert "hola" not in hash_table
    assert "hello" not in hash_table.values
    assert len(hash_table) == 100
```

이렇게 하면 해시 테이블의 기본 목록 크기가 영향을 받지 않는다. 이제 슬롯을 완전히 버리는 대신 빈 슬롯으로 표시하도록 코드를 업데이트해야 한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        index = hash(key) % len(self)
        self.values[index] = BLANK
```

우리가 다시 녹색 단계에 있다면, 우리는 이번 기회에 리팩터링을 하면서 시간을 보낼 수 있다. 동일한 인덱스 공식이 서로 다른 위치에 세 번 나타났다. 코드를 추출하고 코드를 단순화할 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        self.values[self._index(key)] = BLANK

    def __setitem__(self, key, value):
        self.values[self._index(key)] = value

    def __getitem__(self, key):
        value = self.values[self._index(key)]
        if value is BLANK:
            raise KeyError(key)
        return value

    # ...

    def _index(self, key):
        return hash(key) % len(self)
```

이 약간의 수정만 적용하였더니 패턴이 갑자기 나타났다. 항목을 삭제하는 것은 빈 객체를 삽입하는 것과 같다. 따라서 mutator 방법을 활용하여 삭제 루틴을 다시 작성할 수 있을 것이다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        self[key] = BLANK

    # ...
```

대괄호 구문을 통해 값을 할당하면 `.__setitem__()` 메서드로 위임된다. 좋아요, 이제 리팩터링은 그만하자. 이 시점에서 다른 테스트 케이스를 생각하는 것이 훨씬 더 중요하다. 예를 들어 없는 키를 삭제하도록 요청하면 어떻게 될까? Python의 `dict`는 이러한 경우에 `KeyError` 예외를 발생시키므로 다음과 같이 할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_raise_key_error_when_deleting(hash_table):
    with pytest.raises(KeyError) as exception_info:
        del hash_table["missing_key"]
    assert exception_info.value.args[0] == "missing_key"
```

이 테스트 케이스를 다루는 것은 `in` 연산자를 구현할 때 작성한 코드를 따라할 수 있으므로 비교적 간단하다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        if key in self:
            self[key] = BLANK
        else:
            raise KeyError(key)

    # ...
```

해시 테이블에서 키를 찾으면 연결된 값을 sentinel 값으로 덮어써 해당 쌍을 제거한다. 그렇지 않으면 예외가 발생한다. 좋아요, 다음으로 기본 해시 테이블에서 다룰 작업이 하나 더 있다.

## <a name='update-value'>저장된 쌍의 값 갱신</a>
삽입 방법에서 key-value 쌍 업데이트를 이미 다루었으므로 관련 테스트 케이스만 추가하고 예상대로 작동하는지 확인한다.

```python
# test_hashtable.py

# ...

def test_should_update_value(hash_table):
    assert hash_table["hola"] == "hello"

    hash_table["hola"] = "hallo"

    assert hash_table["hola"] == "hallo"
    assert hash_table[98.6] == 37
    assert hash_table[False] is True
    assert len(hash_table) == 100
```

기존 키의 값 `hello`를 `hallo`로 변경한 후 해시 테이블의 길이뿐만 아니라 다른 key-value 쌍이 변경되지 않은 상태로 남아 있는지도 확인한다. 이상. 여러분은 이미 기본 해시 테이블 구현을 하였지만, 비교적 구현 비용이 저렴한 몇 가지 추가 기능은 여전히 누락되어 있다.

## <a name='get-key-value-pairs'>key-value 쌍 가져오기</a>
[방에 있는 코끼리](https://en.wikipedia.org/wiki/Elephant_in_the_room)에게 연설할 시간이다. Python 딕셔너리를 사용하면 항목이라고 하는 키, 값 또는 key-value 쌍에 대해 iterate할 수 있다. 그러나 해시 테이블은 실제로 값에 화려한 색인이 있는 값의 리스트일 뿐이다. 해시 테이블에 저장된 원래 키를 검색하려는 경우 현재 해시 테이블 구현에서 해당 키를 기억하지 못하기 때문에 할 수 없다.

이 섹션에서는 해시 테이블을 크게 리팩터링하여 키와 값을 유지하는 기능을 추가한다. 몇 가지 단계가 포함될 것이고, 그 결과 많은 테스트가 실패할 것이라는 점을 유념하세요. 이러한 중간 단계를 건너뛰고 효과를 보려면 [방어적 복사(defensive copying)](https://realpython.com/python-hash-table/#use-defensive-copying)로 이동하세요.

잠깐만. 이 튜토리얼에서는 **key-value** 쌍에 대해 계속 읽는데, 값을 튜플로 대체하는 것은 어떨까요? 결국, Python에서 튜플은 간단하다. 더 좋은 것은 [명명된 튜플(named tutple)](https://realpython.com/python-namedtuple/)을 사용하여 명명된 요소 검색에 활용할 수 있다는 것이다. 하지만 먼저, 여러분은 테스트를 생각해야 한다.

> **Note:** 테스트 케이스를 파악할 때는 높은 수준에서 사용자 대면 기능에 집중해야 한다. 프로그래머의 경험이나 직감에 따라 예상할 수 있는 코드를 테스트하지 마시오. 테스트가 TDD로 여러분의 구현을 주도해야 한다. 그 반대가 아니다.

먼저, key-value 쌍을 유지하려면 해시 테이블 클래스에 다른 속성이 필요하다.

```python
# test_hashtable.py

# ...

def test_should_return_pairs(hash_table):
    assert ("hola", "hello") in hash_table.pairs
    assert (98.6, 37) in hash_table.pairs
    assert (False, True) in hash_table.pairs
```

이 시점에서 key-value 쌍의 순서는 중요하지 않으므로 요청할 때마다 key-value 쌍이 임의의 순서로 나타날 수 있다고 가정할 수 있다. 그러나 클래스에 다른 필드를 도입하는 대신 `.pairs`로 이름을 바꾸고 기타 필요한 조정을 하여 `.values`를 재사용할 수 있다. 이 경우 구현을 수정할 때까지 일부 테스트는 일시적으로 실패할 수 있다.

`hashtable.py`와 `test_hashtable.py`에서 `.values`를 `.pairs`로 변경하고, 특별한 메서드 `.__setitem__()` 도 수정해야 한다. 특히 지금 키와 연결된 값의 튜플을 저장해야 한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __setitem__(self, key, value):
        self.pairs[self._index(key)] = (key, value)
```

해시 테이블에 요소를 삽입하면 키와 값으로 튜플로 만든 다음 쌍 리스트에서 원하는 인덱스에 해당 튜플을 넣는다. 리스트에는 처음에는 튜플 대신 빈 개체만 저장하므로 쌍 리스트는 서로 다른 두 데이터 타입을 사용한다. 하나는 튜플이고, 다른 하나는 빈 슬롯을 나타내는 것으로 튜플이 아닐 수 있다.

따라서 슬롯을 비어 있는 것으로 표시하는 데 더 이상 특별한 sentinel 상수가 필요하지 않다. `BLANK` 상수를 안전히 제거할 필요가 있으므로 다시 `None`으로 대체하도록 한다. 지금 실행하세요.

> **Note:** 코드를 제거하는 것은 처음에는 받아들이기 어려울 수 있지만, 적은 것이 더 바람직하다! 보시다시피, 시험적인 개발은 때때로 여러분을 제자리로 돌아 오게 할 수 있다.

한 단계 더 물러서서 항목 삭제에 대한 제어 권한을 다시 얻을 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        if key in self:
            self.pairs[self._index(key)] = None
        else:
            raise KeyError(key)
```

불행하게도, 여러분의 `.__delitem_()` 메서드는 더 이상 대괄호 구문을 사용할 수 없다. 이는 불필요한 튜플에서 선택한 sentinel 값을 래핑하는 결과를 초래하기 때문이다. 앞으로 불필요하게 복잡한 논리를 피하려면 여기에 명시적인 할당문을 사용해야 한다.

퍼즐의 마지막 중요한 부분은 `.__getitem__()` 메서드를 업데이트하는 것이다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __getitem__(self, key):
        pair = self.pairs[self._index(key)]
        if pair is None:
            raise KeyError(key)
        return pair[1]
```

여러분은 key-value 쌍을 찾기를 기대하면서 인덱스를 엿본다. 아무것도 얻지 못하면 예외를 발생 시킨다. 반면에, 찾으면, 매핑된 값에 해당하는 색인에서 튜플의 두 번째 요소를 반환한다. 그러나 앞에서 제안한 것처럼 명명된 튜플을 사용하여 보다 우아하게 작성할 수 있다.

```python
# hashtable.py

from typing import NamedTuple, Any

class Pair(NamedTuple):
    key: Any
    value: Any

class HashTable:
    # ...

    def __setitem__(self, key, value):
        self.pairs[self._index(key)] = Pair(key, value)

    def __getitem__(self, key):
        pair = self.pairs[self._index(key)]
        if pair is None:
            raise KeyError(key)
        return pair.value

    # ...
```

`Pair` 클래스는 `.key` 및 `.value` 속성으로 구성되며 [모든 데이터 타입](https://docs.python.org/3/library/typing.html#typing.Any)의 값을 저장할 수 있다. 동시에 클래스는 `NamedTuple` 부모를 확장하므로 정규 튜플의 모든 동작을 상속받는다. `.__getitem__()`에서 명명된 속성 액세스를 이용하려면 `.__setitem__()` 메서드에서 키와 값의 `Pair()`를 명시적으로 호출해야 한다.

> **Note:** key-value 쌍을 나타내는 사용자 정의 데이터 타입을 사용하더라도 두 타입의 호환성 덕분에 쌍 인스턴스 또는 일반 튜플을 예상하는 테스트를 작성할 수 있다.

보고서가 다시 녹색으로 바뀌기 전에 이 시점에서 업데이트해야 하는 몇 가지 테스트 케이스가 있다. 천천히 테스트 스위트를 자세히 검토해 보세요. 또는 막혔다고 느끼면 도움 자료의 코드를 보거나 여기를 살짝 살펴보시오.

```python
# test_hashtable.py

# ...

def test_should_insert_key_value_pairs():
    hash_table = HashTable(capacity=100)

    hash_table["hola"] = "hello"
    hash_table[98.6] = 37
    hash_table[False] = True

    assert ("hola", "hello") in hash_table.pairs
    assert (98.6, 37) in hash_table.pairs
    assert (False, True) in hash_table.pairs

    assert len(hash_table) == 100

# ...

def test_should_delete_key_value_pair(hash_table):
    assert "hola" in hash_table
    assert ("hola", "hello") in hash_table.pairs
    assert len(hash_table) == 100

    del hash_table["hola"]

    assert "hola" not in hash_table
    assert ("hola", "hello") not in hash_table.pairs
    assert len(hash_table) == 100
```

특별히 주의를 요하는 다른 테스트 케이스가 있을 것이다. 특히, 빈 해시 테이블을 만들 때 `None` 값이 없는지 확인하는 것이다. 값 리스트를 쌍 리스트로 바꿉니다. 값을 다시 검색하려면 다음과 같은 list comprehesion 를 사용할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_not_contain_none_value_when_created():
    hash_table = HashTable(capacity=100)
    values = [pair.value for pair in hash_table.pairs if pair]
    assert None not in values
```

만약 여러분이 테스트 케이스에 너무 많은 논리를 넣는 것이 걱정된다면, 여러분이 전적으로 맞는 것이다. 결국 해시 테이블의 동작을 테스트하려고 하는 것이다. 하지만 아직은 그것에 대해 너무 걱정하지 마세요. 잠시 후 이 테스트 케이스를 다시 볼 수 있다.

## <a name='use-defensive-copying'>방어 복사(Defensive Copying) 이용</a>
녹색 단계로 돌아오면, 가능한 코너 케이스(corner case)를 찾아보세요. 예를 들어 `.pairs`는 누구나 의도적으로나 의도하지 않아도 액세스할 수 있는 공개 속성으로 노출된다. 실제로 accessor 메소드는 내부 구현을 누설해서는 안 되며 외부 수정으로부터 변경 가능한 속성을 보호하기 위해 **방어 복사본(defensive copy)**을 만들어야 한다.

```python
# test_hashtable.py

# ...

def test_should_return_copy_of_pairs(hash_table):
    assert hash_table.pairs is not hash_table.pairs
```

해시 테이블에 key-value 쌍을 요청할 때마다 고유한 ID를 가진 새로운 객체를 얻을 수 있다. [Python 속성](https://realpython.com/python-property/) 뒤에 프라이비트 필드를 숨길 수 있으므로 프라이비트 필드를 만들고 `HashTable` 클래스에서만 `.pairs`에 대한 모든 참조를 `._pairs`로 변경한다. 언더스코어로 시작하는 이름은 파이썬의 표준 명명 규칙으로 내부 구현을 나타낸다.

```python
# hashtable.py

# ...

class HashTable:
    def __init__(self, capacity):
        self._pairs = capacity * [None]

    def __len__(self):
        return len(self._pairs)

    def __delitem__(self, key):
        if key in self:
            self._pairs[self._index(key)] = None
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        self._pairs[self._index(key)] = Pair(key, value)

    def __getitem__(self, key):
        pair = self._pairs[self._index(key)]
        if pair is None:
            raise KeyError(key)
        return pair.value

    def __contains__(self, key):
        try:
            self[key]
        except KeyError:
            return False
        else:
            return True

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    @property
    def pairs(self):
        return self._pairs.copy()

    def _index(self, key):
        return hash(key) % len(self)
```

해시 테이블에 저장된 key-value 쌍 목록을 요청하면 매번 얕은 복사본이 제공된다. 해시 테이블의 내부 상태에 대한 참조가 없으므로, 해시 테이블은 해당 복사본의 잠재적인 변경 사항에 영향을 받지 않는다.

> **Note:** 실제 클래스 메소드의 순서는 위에 제시된 코드 블록의 순서와 약간 다를 수 있다. Python의 관점에서 메서드 순서는 중요하지 않기 때문에 괜찮다. 그러나 일반적으로 정적 또는 클래스 메서드로 시작한 다음 클래스의 공용 인터페이스를 확인한다. 내부 구현은 일반적으로 맨 끝에 나타난다.
>
> 코드를 무시하지 않으려면 스토리와 유사한 방식으로 메소드를 구성하는 것이 바람직하다. 특히, 호출되는 낮은 수준의 함수보다 높은 수준의 함수가 먼저 나열되어야 한다.

속성에서 `dict.items()`를 추가로 모방하려면 결과 쌍 리스트에 빈 슬롯이 포함되지 않아야 한다. 즉, 해당 목록에 `None` 값이 있으면 안 된다.

```python
# test_hashtable.py

# ...

def test_should_not_include_blank_pairs(hash_table):
    assert None not in hash_table.pairs
```

이 테스트를 만족시키기 위해 속성의 리스트 comprehension에 조건을 추가하여 빈 값을 필터링할 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def pairs(self):
        return [pair for pair in self._pairs if pair]
```

리스트 comprehension은 새 리스트를 만들기 때문에 `.copy()`에 대한 명시적 호출이 필요하지 않다. key-value 쌍의 원래 리스트에 있는 모든 쌍에 대해 해당 특정 쌍이 진실인지 확인하고 결과를 리스트에 유지한다. 그러나 이렇게 하면 지금 업데이트해야 하는 다른 두 가지 테스트가 필요없다.

```python
# test_hashtable.py

# ...

def test_should_create_empty_value_slots():
    assert HashTable(capacity=3)._pairs == [None, None, None]

# ...

def test_should_insert_none_value():
    hash_table = HashTable(100)
    hash_table["key"] = None
    assert ("key", None) in hash_table.pairs
```

테스트 중 하나가 공개 인터페이스에 초점을 맞추는 대신 내부 구현이기 때문에 이상적이지 않다. 그럼에도 불구하고, 이러한 테스트는 [white-box 테스트](https://en.wikipedia.org/wiki/White-box_testing)로 알려져 있으며, 그 자리를 차지하고 있다.

## <a name='get-keys-and-values'>키와 값 가져오기</a>
key-value 쌍에서 값을 검색하기 위해 list comprehension를 추가하여 수정한 테스트 케이스를 상기하세요. 기억을 되살려야 한다면 여기에 있다.

```python
# test_hashtable.py

# ...

def test_should_not_contain_none_value_when_created():
    hash_table = HashTable(capacity=100)
    values = [pair.value for pair in hash_table.pairs if pair]
    assert None not in values
```

`values = [pair.value for pair in hash_table.pairs if pair]`은 이전 `.pairs`로 대체한 `.values` 속성을 구현하는 데 필요한 것과 같다. 테스트 함수를 업데이트하여 `.values`를 다시 활용할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_not_contain_none_value_when_created():
    assert None not in HashTable(capacity=100).values
```

그것은 헛되게 노력한 것처럼 느껴질 수 있다. 그러나 이러한 값은 이전에는 고정 크기 목록에 저장되었던 반면, 이제는 getter 속성을 통해 동적으로 수행된다. 이 테스트를 만족시키기 위해 list comprehension을 사용하여 이전 구현의 일부를 재사용할 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def values(self):
        return [pair.value for pair in self.pairs]
```

`.pairs` 속성 뒤에는 이미 옵션 필터링 조건이 있으므로 여기서 더 이상 지정할 필요가 없다.

순수주의자들은 값의 순서를 보장할 수 없어 list comprehension 대신 [set comprehension]()를 사용하는 것을 생각할 수 있다. 그러나 이 경우 해시 테이블의 중복 값에 대한 정보가 손실된다. 다른 테스트 케이스를 작성하여 이러한 가능성으로부터 차단하도록 한다.

```python
# test_hashtable.py

# ...

def test_should_return_duplicate_values():
    hash_table = HashTable(capacity=100)
    hash_table["Alice"] = 24
    hash_table["Bob"] = 42
    hash_table["Joe"] = 42
    assert [24, 42, 42] == sorted(hash_table.values)
```

예를 들어 이름과 나이가 있는 해시 테이블이 있고 둘 이상의 사용자가 동일한 나이를 가진 경우 `.values`는 반복되는 모든 나이 값을 유지해야 한다. 나이를 정렬하여 반복 가능한 테스트 실행을 보장할 수 있다. 이 테스트 케이스는 새 코드를 작성할 필요는 없지만 [regressions](https://en.wikipedia.org/wiki/Software_regression)을 방지한다.

기대값, 종류, 갯수 등을 확인해 보는 것이 좋다. 그러나 해시 테이블의 실제 값이 임의의 순서로 나타날 수 있으므로 두 리스트를 직접 비교할 수 없다. 테스트에서 순서를 무시하려면 두 목록을 모두 집합으로 변환하거나 이전과 같이 정렬할 수 있다. 불행히도, 세트는 잠재적인 중복을 제거하지만, 리스트에 호환되지 않는 유형이 포함되어 있으면 정렬할 수 없다.

두 리스트의 순서를 무시하면서 중복 가능성이 있는 임의 타입의 요소가 정확히 동일한지 안정적으로 확인하려면 다음과 같은 Python 관용구를 사용할 수 있다.

```python
def have_same_elements(list1, list2):
    return all(element in list1 for element in list2)
```

내장 [all()](https://realpython.com/python-all/) 함수를 활용하지만, 상당히 장황하다. 여러분은 아마도 [pytest-unordered](https://pypi.org/project/pytest-unordered/) 플러그인을 사용하는 것이 더 나을 것이다. 가상 환경에 설치해야 하는 것에 유념한다.

```
(venv) $ python -m pip install pytest-unordered
```

그런 다음 `unordered()` 함수를 테스트 세트로 가져와 해시 테이블의 값을 래핑하는 데 사용한다.

```python
# test_hashtable.py

import pytest
from pytest_unordered import unordered

from hashtable import HashTable

# ...

def test_should_get_values(hash_table):
    assert unordered(hash_table.values) == ["hello", 37, True]
```

그 결과 값이 순서가 없는 리스트로 변환된다. 이 리스트는 리스트 원소를 비교할 때 순서를 고려하지 않도록 동치성(equality) 테스트 연산자를 재정의한다. 또한 빈 해시 테이블의 값은 빈 리스트이어야 하며 `.values` 속성은 항상 새 리스트 복사본을 반환해야 한다.

```python
# test_hashtable.py

# ...

def test_should_get_values_of_empty_hash_table():
    assert HashTable(capacity=100).values == []

def test_should_return_copy_of_values(hash_table):
    assert hash_table.values is not hash_table.values
```

반면 해시 테이블의 키는 고유해야 하므로 키 리스트가 아닌 키 집합을 반환함으로써 이를 강조하는 것이 합리적이다. 결국, 집합은 정의상 중복이 없는 정렬되지 않은 항목의 컬렉션이다.

```python
# test_hashtable.py

# ...

def test_should_get_keys(hash_table):
    assert hash_table.keys == {"hola", 98.6, False}

def test_should_get_keys_of_empty_hash_table():
    assert HashTable(capacity=100).keys == set()

def test_should_return_copy_of_keys(hash_table):
    assert hash_table.keys is not hash_table.keys
```

Python에는 공 집합 리터럴이 없기 때문에 이 경우에는 내장 `set()` 함수를 직접 호출해야 한다. 해당 getter 함수의 구현은 친숙해 보일 것이다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def keys(self):
        return {pair.key for pair in self.pairs}
```

`.values` 속성과 비슷하다. 차이점은 대괄호 대신 중괄호를 사용하고 명명된 튜플에서 `.value` 대신 `.key` 속성을 참조한다. 또는 원하는 경우 `pair[0]`을(를) 사용할 수 있지만 읽기가 더 어려워 보인다.

또한 `.pairs` 속성을 다룰 때 놓쳤던 유사한 테스트 케이스가 필요하다는 것을 알려준다. 일관성을 위해 쌍 집합을 반환하는 것이 합리적이다.

```python
# test_hashtable.py

# ...

def test_should_return_pairs(hash_table):
    assert hash_table.pairs == {
        ("hola", "hello"),
        (98.6, 37),
        (False, True)
    }

def test_should_get_pairs_of_empty_hash_table():
    assert HashTable(capacity=100).pairs == set()
```

따라서 `.pairs` 속성은 지금부터 set comprehension를 사용한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def pairs(self):
        return {pair for pair in self._pairs if pair}
```

각 key-value 쌍이 고유하므로 정보가 손실될 염려가 없다. 이 시점에서, 여러분은 다시 녹색 단계에 있어야 한다.

`.pairs` 속성을 사용하여 해시 테이블을 일반 이전 사전으로 변환하고 `.keys`와 `.values`를 사용하여 다음을 테스트할 수 있다.

```python
def test_should_convert_to_dict(hash_table):
    dictionary = dict(hash_table.pairs)
    assert set(dictionary.keys()) == hash_table.keys
    assert set(dictionary.items()) == hash_table.pairs
    assert list(dictionary.values()) == unordered(hash_table.values)
```

요소의 순서를 무시하려면 비교하기 전에 사전 키와 key-value 쌍을 집합으로 변환하여야 한다. 반대로 해시 테이블의 값은 리스트로 표현되므로 요소 순서를 무시하고 리스트를 비교하려면 `unordered()` 함수를 사용해야 한다.

축하합니다, 여러분의 해시 테이블이 이제 정말로 모양을 갖추기 시작했어요!

## <a name='report-hashtable-length'>해시 테이블의 크기</a>
여러분은 지금까지 단순함을 위해 일부러 아직 고려하지 않은 작은 세부 사항이 하나 있다. 이는 해시 테이블의 크기로, 현재 빈 슬롯만 있는 경우에도 최대 용량을 보고한다. 다행히도, 이것을 수정하는데 많은 노력이 필요하지 않다. `test_should_report_capacity()`라는 함수를 찾아 아래와 같이 이름을 바꾸고 빈 해시 테이블의 길이가 `100`이 아닌 `0`인지 확인한다.

```python
# test_hashtable.py

# ...

def test_should_report_length_of_empty_hash_table():
    assert len(HashTable(capacity=100)) == 0
```

용량을 크기와 독립적으로 하려면 특별한 메서드 `.__len__()`를 수정하여 모든 슬롯의 private 리스트 대신 필터링된 쌍이 있는 공용 속성을 참조한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __len__(self):
        return len(self.pairs)

    # ...
```

방금 변수 이름 앞의 밑줄을 제거했다. 그러나 그 작은 변화는 이제 많은 시험들이 오류와 함께 갑자기 끝나고 몇몇 시험들을 실패하게 하고 있다.

> **Note:** 실패한 테스트는 assertion이 `False`으로 평가되기 때문에 덜 심각하지만, 오류는 코드에서 전혀 예상 못한 연산을 수행한다.

대부분의 테스트 케이스는 키를 인덱스에 매핑할 때 `division by zero`처럼 처리 안된 예외로 인해 어려움을 겪을 것으로 보인다. 이것은 `._index()`가 해시 테이블의 크기를 사용하여 해시 키를 사용 가능한 슬롯 수로 나눈 나머지를 찾기 때문이다. 그러나 이제 해시 테이블의 길이는 다른 의미를 갖는다. 대신 내부 리스트의 길이를 선택해야 한다.

```python
# hashtable.py

class HashTable:
    # ...

    def _index(self, key):
        return hash(key) % len(self._pairs)
```

이제 많이 좋아졌어요. 여전히 실패하는 세 가지 테스트 케이스는 해시 테이블의 크기에 대한 잘못된 가정을 사용한다. 다음과 같이 가정을 변경하면 테스트를 통과할 것이다.

```python
# test_hashtable.py

# ...

def test_should_insert_key_value_pairs():
    hash_table = HashTable(capacity=100)

    hash_table["hola"] = "hello"
    hash_table[98.6] = 37
    hash_table[False] = True

    assert ("hola", "hello") in hash_table.pairs
    assert (98.6, 37) in hash_table.pairs
    assert (False, True) in hash_table.pairs

    assert len(hash_table) == 3

# ...

def test_should_delete_key_value_pair(hash_table):
    assert "hola" in hash_table
    assert ("hola", "hello") in hash_table.pairs
    assert len(hash_table) == 3

    del hash_table["hola"]

    assert "hola" not in hash_table
    assert ("hola", "hello") not in hash_table.pairs
    assert len(hash_table) == 2

# ...

def test_should_update_value(hash_table):
    assert hash_table["hola"] == "hello"

    hash_table["hola"] = "hallo"

    assert hash_table["hola"] == "hallo"
    assert hash_table[98.6] == 37
    assert hash_table[False] is True
    assert len(hash_table) == 3
```

다시 돌아 왔지만 `ZeroDivisionError`는 즉시 테스트 케이스를 추가해야 하는 빨간색 플래그였다.

```python
# test_hashtable.py

# ...

def test_should_not_create_hashtable_with_zero_capacity():
    with pytest.raises(ValueError):
        HashTable(capacity=0)

def test_should_not_create_hashtable_with_negative_capacity():
    with pytest.raises(ValueError):
        HashTable(capacity=-100)
```

용량이 양수가 아닌 해시 테이블을 만드는 것은 그다지 의미가 없다. 어떻게 마이너스 크기의 용기를 가질 수 있나요? 그래서 누군가가 의도적이거나 우연히 그런 시도를 한다면 여러분은 예외를 제기해야 한다. Python에서 이러한 잘못된 입력 인수를 나타내는 일반적인 방법은 `ValueError` 예외를 발생시키는 것이다.

```python
# hashtable.py

# ...

class HashTable:
    def __init__(self, capacity):
        if capacity < 1:
            raise ValueError("Capacity must be a positive number")
        self._pairs = capacity * [None]

    # ...
```

만약 여러분이 부지런하다면, 여러분은 아마도 잘못된 인수 타입도 확인해야 할 것이지만, 이는 튜토리얼의 범위를 벗어난다. 여러분은 그것을 스스로 할 수 있을 것이다.

그런 다음 `pytest`를 통해 픽스처로 제공하고 사용하고 있는 해시 테이블의 사용 크기를 테스트하기 위한 다른 시나리오를 추가한다.

```python
# test_hashtable.py

# ...

def test_should_report_length(hash_table):
    assert len(hash_table) == 3
```

세 key-value 쌍이 있으므로 해시 테이블의 크기도 '3'이어야 한다. 이 테스트에는 추가 코드가 필요하지 않다. 마지막으로, 현재 리팩토링 단계에 있으므로 `.capacity` 속성을 도입하고 가능한 경우 이를 사용하여 [syntatic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar)을 약간 추가할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_report_capacity_of_empty_hash_table():
    assert HashTable(capacity=100).capacity == 100

def test_should_report_capacity(hash_table):
    assert hash_table.capacity == 100
```

용량은 일정하며 해시 테이블 생성 시 결정된다. 이는 쌍 리스트의 길이에서 구할 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def capacity(self):
        return len(self._pairs)

    def _index(self, key):
        return hash(key) % self.capacity
```

문제 영역에 새 어휘를 도입하면 보다 정확하고 명확한 이름을 지정하기 위한 새로운 기회를 발견할 수 있다. 예를 들어 해시 테이블에 저장된 실제 key-value 쌍과 쌍에 대한 내부 슬롯 리스트를 모두 참조하기 위해 상호 교환적으로 사용되는 단어 쌍을 보았다. 모든 곳에서 `._pairs`를 `._slots`으로 변경하여 코드를 리팩터링할 수 있는 좋은 기회가 될 수 있다.

그런 다음 이전 테스트 사례 중 하나가 그 의도를 보다 명확하게 전달할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_create_empty_value_slots():
    assert HashTable(capacity=3)._slots == [None, None, None]
```

단어 값을 쌍으로 대체하여 이 테스트의 이름을 변경하는 것이 훨씬 더 합리적일 수 있다.

```python
# test_hashtable.py

# ...

def test_should_create_empty_pair_slots():
    assert HashTable(capacity=3)._slots == [None, None, None]
```

여러분은 그러한 철학적인 사색이 필요하지 않다고 생각할지도 모른다. 그러나 이름을 더 자세히 설명할수록 코드를 더 쉽게 읽을 수 있다. 다른 개발자의 코드가 아니라면 나중에 여러분의 코드를 더 쉽게 읽을 수 있다. 심지어 그것에 대한 책과 농담도 있다. 테스트는 문서화의 한 형태이므로 테스트 중인 코드와 동일한 수준의 세부 정보에 대한 주의를 유지하는 것이 좋다.

## <a name='make-hash-table-iterable'>해시 테이블을 iterable로 만들기</a>
Python에서는 키, 값 또는 항목으로 알려진 key-value 쌍을 통해 [사전을 사용한 반복문](https://realpython.com/iterate-through-dictionary-python/)을 수행할 수 있다. 사용자 정의 해시 테이블에서 동일한 연산을 수행하려면 몇 가지 테스트 사례를 스케치하는 것부터 시작한다.

```python
# test_hashtable.py

# ...

def test_should_iterate_over_keys(hash_table):
    for key in hash_table.keys:
        assert key in ("hola", 98.6, False)

def test_should_iterate_over_values(hash_table):
    for value in hash_table.values:
        assert value in ("hello", 37, True)

def test_should_iterate_over_pairs(hash_table):
    for key, value in hash_table.pairs:
        assert key in hash_table.keys
        assert value in hash_table.values
```

키, 값 또는 key-value 쌍에 의한 반복은 기본 세트와 리스트가 이미 이를 처리할 수 있기 때문에 현재 구현에서 즉시 작동합니다. `HashTable`의 인스턴스를 [반복 가능하게(iterable)](https://docs.python.org/3/glossary.html#term-iterable) 만들려면 [for loop](https://realpython.com/python-for-loop/)에서 직접 사용할 수 있도록 특별한 메서드를 정의해야 한다.

```python
# test_hashtable.py

# ...

def test_should_iterate_over_instance(hash_table):
    for key in hash_table:
        assert key in ("hola", 98.6, False)
```

이전과 달리 여기서 `HashTable` 인스턴스에 대한 참조를 넘겨주지만 동작은 `.keys` 속성을 반복하는 것과 동일하다. 이 연산은 Python의 내장 `dict`와 호환된다.

필요한 특별한 메서드인 `.__iter__()`는 [iterator 객체](https://docs.python.org/3/glossary.html#term-iterator)를 반환해야 하며 루프는 내부적으로 이를 사용한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __iter__(self):
        yield from self.keys
```

이것은 Python의 [yield](https://realpython.com/introduction-to-python-generators/) 키워드를 활용하는 [generator iterator](https://docs.python.org/3/glossary.html#term-generator-iterator)의 예이다.

> **Note:** [yield from](https://docs.python.org/3/whatsnew/3.3.html#pep-380) 표현식은 반복을 하위 생성기(subgenerator)로 위임하는데, 이는 리스트나 집합과 같은 다른 반복 가능한 개체가 될 수 있다.

yield 키워드를 사용하면 다른 클래스를 만들지 않고 함수 스타일을 사용하여 내부 iterator를 정의할 수 있다. 시작될 때 `for` 루프는 `__iter__()`라는 이름의 특별한 메서드를 호출한다.

이 시점에서 해시 테이블에는 몇 가지 중요하지 않은 함수만 누락되어 있다.

## <a name='represent-hashtable-text'>해시 테이블을 텍스트로 표현</a>
이 섹션에서 구현할 다음 기능은 표준 출력에 인쇄할 때 해시 테이블을 깔끔하게 보이도록 한다. 해시 테이블의 텍스트 표현은 Python `dict` 리터럴과 유사하다.

```python
# test_hashtable.py

# ...

def test_should_use_dict_literal_for_str(hash_table):
    assert str(hash_table) in {
        "{'hola': 'hello', 98.6: 37, False: True}",
        "{'hola': 'hello', False: True, 98.6: 37}",
        "{98.6: 37, 'hola': 'hello', False: True}",
        "{98.6: 37, False: True, 'hola': 'hello'}",
        "{False: True, 'hola': 'hello', 98.6: 37}",
        "{False: True, 98.6: 37, 'hola': 'hello'}",
    }
```

리터럴은 중괄호, 쉼표와 콜론을 사용하고 키와 값은 각각 표현된다. 예를 들어 문자열은 단일 아포스트로피로 둘러싸여 있다. key-value 쌍의 정확한 순서를 모르기 때문에 해시 테이블의 문자열 표현이 가능한 쌍 순열 중 하나와 동일한지 확인한다.

클래스를 내장 `str()` 함수로 작동시키려면 `HashTable`에서 해당하는 특별한 메서드를 구현해야 한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __str__(self):
        pairs = []
        for key, value in self.pairs:
            pairs.append(f"{key!r}: {value!r}")
        return "{" + ", ".join(pairs) + "}"
```

`.pairs` 속성을 통해 키와 값을 반복하고 [f-string](https://realpython.com/python-f-strings/)을 사용하여 개별 쌍의 포맷을 지정한다. 템플릿 문자열에서  `!r` 변환 플래그를 확인하세오. 이 플래그는 키와 값에 대한 기본 `str()` 대신 `repr()`을 호출하도록 한다. 이렇게 하면 데이터 타입에 따라 다른 보다 명확한 표현이 보장된다. 예를 들어, 문자열을 단일 아포스트로피로 묶는다.

`str()`과 `repr()`의 차이는 더 미묘하다. 일반적으로 둘 다 객체를 문자열로 변환하기 위한 것이다. 그러나 `str()`이 사람에게 친숙한 텍스트로 반환할 것으로 예상할 수 있지만 `repr()`은 종종 원래 객체를 다시 만들기 위해 수행할 수 있는 유효한 Python 코드를 반환한다.

```python
>>> from fractions import Fraction
>>> quarter = Fraction("1/4")

>>> str(quarter)
'1/4'

>>> repr(quarter)
'Fraction(1, 4)'

>>> eval(repr(quarter))
Fraction(1, 4)
```

이 예제에서 [Python fraction](https://realpython.com/python-fractions/)의 문자열 표현은 '1/4'이지만 동일한 객체의 표준 문자열 표현은 `Fraction` 클래스에 대한 호출을 나타낸다.

`HashTable` 클래스에서도 유사한 효과를 얻을 수 있다. 안타깝게도 클래스 이니셜라이저에서는 현재 값으로부터 새 인스턴스를 만들 수 없다. Python 딕셔너리에서 `HashTable` 인스턴스를 만들 수 있는 [클래스 메서드](https://realpython.com/instance-class-and-static-methods-demystified/#delicious-pizza-factories-with-classmethod)를 도입하여 이 문제를 해결할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_create_hashtable_from_dict():
    dictionary = {"hola": "hello", 98.6: 37, False: True}

    hash_table = HashTable.from_dict(dictionary)

    assert hash_table.capacity == len(dictionary) * 10
    assert hash_table.keys == set(dictionary.keys())
    assert hash_table.pairs == set(dictionary.items())
    assert unordered(hash_table.values) == list(dictionary.values())
```

이것은 반대 방향이었던 이전과 잘 어울린다. 그러나 해시 테이블은 원래 딕셔너리의 모든 key-value 쌍을 유지할 수 있도록 충분히 큰 크기를 가정해야 한다. 합리적인 추정치는 쌍 갯수의 10배인 것 같다. 지금은 이를 하드 코딩할 수 있다.

```python
# hashtable.py

# ...

class HashTable:

    @classmethod
    def from_dict(cls, dictionary):
        hash_table = cls(len(dictionary) * 10)
        for key, value in dictionary.items():
            hash_table[key] = value
        return hash_table
```

새 해시 테이블을 생성하고 임의 인수를 사용하여 크기를 설정한다. 그런 다음 메서드의 인수로 전달된 딕셔너리에서 key-value 쌍을 복사하여 삽입한다. 원하는 경우 기본 용량을 재정의할 수 있으므로 유사한 테스트 케이스를 추가하세오.

```python
# test_hashtable.py

# ...

def test_should_create_hashtable_from_dict_with_custom_capacity():
    dictionary = {"hola": "hello", 98.6: 37, False: True}

    hash_table = HashTable.from_dict(dictionary, capacity=100)

    assert hash_table.capacity == 100
    assert hash_table.keys == set(dictionary.keys())
    assert hash_table.pairs == set(dictionary.items())
    assert unordered(hash_table.values) == list(dictionary.values())
```

용량을 선택적으로 만들기 위해 부울식의 [short-circuit evaluation](https://en.wikipedia.org/wiki/Short-circuit_evaluation)를 이용할 수 있다.

```python
# hashtable.py

# ...

class HashTable:

    @classmethod
    def from_dict(cls, dictionary, capacity=None):
        hash_table = cls(capacity or len(dictionary) * 10)
        for key, value in dictionary.items():
            hash_table[key] = value
        return hash_table
```

`capacity`를 지정하지 않으면 사전의 크기에 10을 곱하는 기본 연산으로 돌아간다. 이를 통해 `HashTable` 인스턴스에 표준 문자열 표현을 제공할 수 있다.

```python
# test_hashtable.py

# ...

def test_should_have_canonical_string_representation(hash_table):
    assert repr(hash_table) in {
        "HashTable.from_dict({'hola': 'hello', 98.6: 37, False: True})",
        "HashTable.from_dict({'hola': 'hello', False: True, 98.6: 37})",
        "HashTable.from_dict({98.6: 37, 'hola': 'hello', False: True})",
        "HashTable.from_dict({98.6: 37, False: True, 'hola': 'hello'})",
        "HashTable.from_dict({False: True, 'hola': 'hello', 98.6: 37})",
        "HashTable.from_dict({False: True, 98.6: 37, 'hola': 'hello'})",
    }
```

이전과 마찬가지로 결과 표현을 가능한 모든 속성 순서 순열로 확인한다. 해시 테이블의 표준 문자열 표현은 `str()`과 거의 동일하게 보이지만 새로운 `.from_dict()` 클래스 메서드를 호출하여 `dict` 리터럴을 래핑한다. 해당 구현은 내장 `str()` 함수를 호출하여 특별한 메서드 `.__str_()`에 위임한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __repr__(self):
        cls = self.__class__.__name__
        return f"{cls}.from_dict({str(self)})"
```

나중에 클래스 이름을 변경할 경우 문제가 발생하지 않도록 클래스 이름을 하드 코딩하지 않았다.

이 장의 도입부에서 언급한 리스트에서 거의 모든 핵심 기능과 비필수 기능을 구현했기 때문에 해시 테이블 프로토타입이 거의 준비되었다. 방금 Python `dict`에서 해시 테이블을 생성하는 기능을 추가하고 해당 인스턴스에 대한 문자열 표현을 제공했다. 마지막은 해시 테이블 인스턴스를 값으로 비교할 수 있도록 하는 것이다.

## <a name='test-equality-hashtables'>해시 테이블의 동치성(Equality) 테스트</a>
해시 테이블은 구성 요소들에 특정 순서를 부여하지 않는다는 점에서 집합과 같다. 실제로 해시 테이블과 집합은 모두 해시 함수에 의해 지원되기 때문에 생각보다 많은 공통점을 갖고 있다. 해시 테이블의 key-value 쌍을 정렬되지 않게 만드는 것은 해시 함수이다. 그러나 파이썬 3.6부터 `dict`는 실제로 구현 세부사항으로 삽입 순서를 유지한다.

두 해시 테이블은 key-value 쌍의 집합이 같을 경우에만 같다고 할 수 있다. 그러나 Python은 사용자 정의 데이터 타입의 값을 해석하는 방법을 모르기 때문에 기본적으로 개체 ID를 비교한다. 따라서 해시 테이블의 두 인스턴스가 같은 key-vlaue 쌍 집합을 공유하더라도 항상 같다고 하지 않는다.

이 문제를 해결하려면 해시 테이블 클래스의 특별한 메서드 `.__eq__()`를 제공하여 동치성 검정 연산자(==)를 구현할 수 있다. 또한 Python은 사용자가 `.__ne__()`를 명시적으로 구현하지 않는 한 동등하지 않은 연산자(`!=`)를 평가하기 위해 이 메서드를 호출한다.

해시 테이블의 순서에 관계없이 동일한 key-vlaue 쌍을 가진 해시 테이블이 자체, 해당 복사본 또는 다른 인스턴스와 동일하도록 할 수 있다. 반대로, 해시 테이블은 key-vlaue 쌍의 다른 집합 또는 완전히 다른 데이터 타입을 가진 인스턴스와 같지 않아야 한다.

```python
# test_hashtable.py

# ...

def test_should_compare_equal_to_itself(hash_table):
    assert hash_table == hash_table

def test_should_compare_equal_to_copy(hash_table):
    assert hash_table is not hash_table.copy()
    assert hash_table == hash_table.copy()

def test_should_compare_equal_different_key_value_order(hash_table):
    h1 = HashTable.from_dict({"a": 1, "b": 2, "c": 3})
    h2 = HashTable.from_dict({"b": 2, "a": 1, "c": 3})
    assert h1 == h2

def test_should_compare_unequal(hash_table):
    other = HashTable.from_dict({"different": "value"})
    assert hash_table != other

def test_should_compare_unequal_another_data_type(hash_table):
    assert hash_table != 42
```

이전에 소개한 `.from_dict()`를 사용하여 새 해시 테이블에 값을 빠르게 채운다. 동일한 클래스 메서드를 사용하여 해시 테이블 인스턴스의 새 복사본을 만들 수 있다. 다음은 이러한 테스트 사례를 만족시키는 코드이다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __eq__(self, other):
        if self is other:
            return True
        if type(self) is not type(other):
            return False
        return set(self.pairs) == set(other.pairs)

    # ...

    def copy(self):
        return HashTable.from_dict(dict(self.pairs), self.capacity)
```

특별한 메서드 `.__eq_()`는 비교할 객체를 인수로 사용한다. 해당 객체가 `HashTable`의 현재 인스턴스인 경우 동일한 ID이라면 value가 일치함을 의미하므로 `True`를 반환한다. 그렇지 않으면 타입과 key-value 쌍 집합을 비교하며 계속 진행한다. 향후 `.pairs`를 다른 순서 리스트로 변경하더라도 `sets`으로 변환하면 순서에 상관이 없다.

참고로, 결과 복사본은 key-value 쌍뿐만 아니라 소스 해시 테이블과 동일한 용량을 가져야 한다. 동시에 용량이 다른 두 해시 테이블 또한 여전히 동일하게 비교되어야 한다.

```python
# test_hashtable.py

# ...

def test_should_copy_keys_values_pairs_capacity(hash_table):
    copy = hash_table.copy()
    assert copy is not hash_table
    assert set(hash_table.keys) == set(copy.keys)
    assert unordered(hash_table.values) == copy.values
    assert set(hash_table.pairs) == set(copy.pairs)
    assert hash_table.capacity == copy.capacity

def test_should_compare_equal_different_capacity():
    data = {"a": 1, "b": 2, "c": 3}
    h1 = HashTable.from_dict(data, capacity=50)
    h2 = HashTable.from_dict(data, capacity=100)
    assert h1 == h2
```

이 두 가지 테스트는 현재 `HashTable` 구현과 함께 작동하므로 추가로 코딩할 필요가 없다.

사용자 정의 해시 테이블 프로토타입에는 기본 사전이 제공하는 몇 가지 필수적이지 않은 기능은 아직 없다. 여러분이 그것들을 연습으로 직접 추가는 것을 시도해 볼 수 있다. 예를 들어, `dict.clear()` 또는 `dict.update()`와 같은 다른 메서드를 Python 사전에서 복제할 수 있다. 그 외에도 Python 3.9 이후 `dict`에서 지원되는 [bitwise operators](https://realpython.com/python-bitwise-operators/) 중 하나를 구현할 수 있으며, 이를 통해 [union 연산](https://www.python.org/dev/peps/pep-0584/)이 가능하다.

잘 했어요! 이것은 튜토리얼용 테스트 세트이며, 해시 테이블은 모든 단위 테스트를 통과했다. 그것은 대단한 일이었기 때문에 자신의 등을 두드려 마땅하다. 벌써 했나요?

삽입된 쌍만 설명하기 위해 해시 테이블의 용량을 줄였다고 가정한다. 다음 해시 테이블은 소스 사전에 저장된 세 개의 key-value 쌍을 모두 수용해야 한다.

```python
>>> from hashtable import HashTable
>>> source = {"hola": "hello", 98.6: 37, False: True}
>>> hash_table = HashTable.from_dict(source, capacity=len(source))
>>> str(hash_table)
'{False: True}'
```

그러나 결과 해시 테이블의 키와 값을 표시하면 항목 수가 더 적을 수 있다. 이 코드 조각을 반복 가능하게 만들려면 `PYTONHASHSEED` 환경 변수를 0으로 설정하여 해시 랜덤화를 비활성화한 상태로 실행하세요.

해시 테이블에 사용 가능한 공간이 충분하더라도 정보가 손실되는 경우가 많다. 그것은 대부분의 해시 함수가 완벽하지 않아서 해시 충돌을 일으키는데, 다음에 해결하는 방법을 다루게 될 것이다.
