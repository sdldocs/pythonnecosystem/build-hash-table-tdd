더 깊이 들어가기 전에, 여러분은 용어가 약간 혼란스러울 수 있기 때문에 그것에 익숙해야 한다. 구어적으로 **해시 테이블(hash table)** 또는 **해시 맵(hash map)**이라는 용어는 종종 **딕셔너리(dictionary)**라는 단어와 상호 교환적으로 사용된다. 그러나 전자가 후자보다 더 구체적이며, 두 개념 사이에는 미묘한 차이가 있다.

## <a name='hash-table-vs-dictionary'>해시 테이블과 사전</a>
컴퓨터 과학에서 [사전](https://en.wikipedia.org/wiki/Associative_array)은 **키(key)**와 **값(value)**이 쌍으로 배열된 [추상 데이터 타입(abstract data type)](https://en.wikipedia.org/wiki/Abstract_data_type)이다. 또한 이는 그 요소에 대해 다음과 같은 연산을 정의하고 있다.

- key-value 쌍 추가
- key-value 쌍 삭제
- key-value 쌍 업데이트
- 지정된 키와 연결된 값 찾기

어떤 의미에서, 이 추상 데이터 타입은 **2개 국어 사전**과 유사하며, 키는 외국어 단어이고, 값은 단어의 정의 또는 다른 언어로의 번역이다. 그러나 키와 값 사이에 항상 동등한 느낌이 있어야 하는 것은 아니다. **전화번호부**는 이름과 해당 전화번호를 결합한 사전의 또 다른 예라 할 수 있다.

> **Note:** 어떤 것을 다른 것에 `매핑(map)`하거나 값을 키와 `연결(associate)`할 때마다 기본적으로 일종의 사전을 사용한다. 그렇기 때문에 사전은 **맵(map)** 또는 `연관 배열(associative array)`로도 알려져 있다.

사전은 몇 가지 흥미로운 속성을 가지고 있다. 그 중 하나는 사전을 하나 이상의 인수를 정확히 하나의 값에 투영하는 [수학적 함수](https://en.wikipedia.org/wiki/Function_(mathematics))로 생각할 수 있다는 것이다. 그 사실의 직접적인 결과는 다음과 같다.

- **key-value 쌍만**: 사전에 값이 없거나 다른 방법으로 키를 가질 수 없다. 그들은 항상 함께 한다.
- **임의 키와 값**: 키와 값은 동일하거나 다른 타입의 분리된 두 집합에 속할 수 있다. 키와 값 모두 숫자, 단어 또는 심지어 사진과 같은 거의 모든 타입일 수 있다.
- **정렬되지 않은 key-value 쌍**: 마지막으로 사전에는 일반적으로 key-value 쌍에 대한 순서가 없다. 그러나 이는 구현에 따라 다를 수 있다.
- **고유 키**: 사전은 함수의 정의를 위반하므로 중복 키를 포함할 수 없다.
- **고유하지 않은 값**: 동일한 값을 여러 키와 연결할 수 있지만 꼭 그럴 필요는 없다.

사전의 개념을 확장하는 관련 개념들이 있다. 예를 들어, [멀티맵(multimap)](https://en.wikipedia.org/wiki/Multimap)을 사용하면 키당 둘 이상의 값을 가질 수 있는 반면 [양방향 맵(bidirectional map)](https://en.wikipedia.org/wiki/Bidirectional_map)은 키를 값에 매핑할 뿐만 아니라 반대 방향의 매핑도 제공한다. 그러나 이 튜토리얼에서는 각 키에 정확히 하나의 값을 매핑하는 일반 사전만 고려한다.

다음 그림은 가상 사전을 그래픽으로 묘사한 것으로, 일부 추상적인 개념을 해당 영어 단어에 매핑한 것이다.

![Fig 1.1](./images/fig-1-1.webp)
<center>Fig. 1-1 사전 추상 데이터 타입의 그래픽 설명</center>
<div style="text-align: right">(source: https://realpython.com/python-hash-table/)</div>

그것은 두 개의 완전히 다른 요소들의 집합인 키에서 값으로 매핑하는 단일 방향 맵이다. 바로, `bow`라는 단어는 여러 의미를 가진 [동음 이의어](https://en.wikipedia.org/wiki/Homonym)이기 때문에 키보다 적은 값을 가질 수 있다. 그러나 개념적으로 이 사전은 여전히 네 쌍을 포함하고 있다. 구현 방법에 따라 반복되는 값을 재사용하여 메모리를 절약하거나 단순성을 위해 복제할 수 있다.

이제 그런 사전을 프로그래밍 언어로 어떻게 코딩할까? 정답은 그렇지 않다는 것이다. 왜냐하면 대부분의 현대 프로그래밍 언어는 [기본 데이터 타입(primitive data type)](https://en.wikipedia.org/wiki/Primitive_data_type) 또는 [표준 라이브러리](https://en.wikipedia.org/wiki/Standard_library)의 클래스로 사전을 함께 제공되기 때문이다. Python은 사전을 직접 작성할 필요가 없도록 C로 작성된 고도로 최적화된 데이터 구조로 이미 구현된 내장 딕셔너리 타입을 지원한다.

Python의 `dict`를 사용하면 이 섹션의 시작 부분에 나열된 모든 사전 작업을 수행할 수 있다.

```python
>>> glossary = {"BDFL": "Benevolent Dictator For Life"}
>>> glossary["GIL"] = "Global Interpreter Lock"  # Add
>>> glossary["BDFL"] = "Guido van Rossum"  # Update
>>> del glossary["GIL"]  # Delete
>>> glossary["BDFL"]  # Search
'Guido van Rossum'
>>> glossary
{'BDFL': 'Guido van Rossum'}
```

**대괄호 구문**([ ])을 사용하여 딕셔너리에 새로운 key-value 쌍을 추가할 수 있다. 키로 식별된 기존 쌍의 값을 업데이트하거나 삭제할 수도 있다. 마지막으로 지정된 키와 관련된 값을 조회할 수 있다.

그렇긴 하지만 다른 질문을 할 수도 있다. 내장된 딕셔너리는 실제로 어떻게 작동할까? 어떻게 임의의 데이터 타입의 키를 매핑하고, 어떻게 그렇게 빨리 매핑할 수 있을까?

이러한 추상적인 데이터 타입의 효율적인 구현을 찾는 것은 **딕셔너리 문제**로 알려져 있다. 가장 잘 알려진 솔루션 중 하나는 우리가 다루고자 하는 [해시 테이블](https://en.wikipedia.org/wiki/Hash_table) 데이터 구조를 활용하는 것이다. 그러나, 이것이 일반적으로 딕셔너리를 구현하는 유일한 방법은 아니다. 또 다른 인기 있는 방법은 [red-black tree](https://en.wikipedia.org/wiki/Red%E2%80%93black_tree)를 이용하여 구현하는 것이다.

## <a name='hash-table'>해시 테이블: 해시 함수가 있는 배열</a>
여러분은 요청하는 인덱스에 상관없이 Python의 [시퀀스](https://docs.python.org/3/glossary.html#term-sequence) 요소를 액세스하는 것이 왜 그렇게 빨리 작동하는지 궁금해 본 적이 있나요? 다음과 같은 매우 긴 문자열로 작업했다고 가정해 보자.

```python
>>> import string
>>> text = string.ascii_uppercase * 100_000_000

>>> text[:50]  # Show the first 50 characters
'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX'

>>> len(text)
2600000000
```

위의 텍스트 변수에는 [ASCII](https://en.wikipedia.org/wiki/ASCII) 문자를 반복하는 것으로 26억 개의 문자가 있으며, 이는 [Python의 len() 함수](https://realpython.com/len-python-function/)로 계산할 수 있다. 그러나 이 문자열에서 첫 번째, 중간, 마지막 또는 다른 문자를 얻는 것도 마찬가지로 빠르다.

```python
>>> text[0]  # The first element
'A'

>>> text[len(text) // 2]  # The middle element
'A'

>>> text[-1]  # The last element, same as text[len(text) - 1]
'Z'
```

리스트와 튜플과 같은 Python의 시퀀스 타입에 대해서도 마찬가지이다. 왜요? 이처럼 빠른 속도의 비밀은 Python의 시퀀스가 랜덤 액세스 데이터 구조인 배열에 의해 지원되기 때문이다. 다음 두 원칙을 따른다.

1. 배열에 **연속된** 메모리 블록을 할당한다.
2. 배열의 모든 요소의 크기는 **고정**되어 있다.

**오프셋**이라는 배열의 메모리 주소를 알면, 간단한 공식으로 배열에서 원하는 요소에 즉시 도달할 수 있다.

> 시퀀스 요소의 메모리 주소 계산법<br>
> `element_address` = `offset` + (`element_size` x `element_index`)

배열의 오프셋에서 시작한다. 이 오프셋은 배열의 첫 번째 요소의 주소이기도 하며 인덱스는 `0`이다. 그런 다음 필요한 바이트 수를 추가하여 앞으로 이동한다. 이 바이트 수는 요소 크기에 대상 요소의 인덱스를 곱하여 얻을 수 있다. 몇 개의 숫자를 곱하고 더하는 데는 항상 같은 시간이 걸린다.

> **Note:** 배열과 달리 Python의 리스트는 다양한 크기의 다른 타입의 요소를 포함할 수 있으며, 이는 위의 공식을 깨뜨릴 수 있다. 이를 완화하기 위해 Python은 배열에 직접 값을 저장하는 대신 메모리 위치를 가리키는 [포인터(또는 reference)](https://realpython.com/pointers-in-python/) 배열을 도입하여 다른 수준의 간접적인 기능을 추가한다.

> ![Fig 1.2](./images/fig-1-2.webp)
> <center>Fig. 1-1 딕셔너리 추상 데이터 타입의 그래픽 설명</center>
> <div style="text-align: right">(source: https://realpython.com/python-hash-table/)</div>
>
> 포인터는 항상 같은 양의 공간을 차지하는 정수에 불과하다. 일반적으로 16진수 표기법을 사용하여 메모리 주소를 나타낸다. Python을 비롯한 몇몇 언어들은 이러한 숫자 앞에 `0x`를 붙인다.

자, 그러면 배열에서 요소를 찾는 것이 물리적으로 어디에 있는지에 관계없이 신속하다는 것을 알 수 있을 것이다. 여러분은 이같은 방법으로 딕셔너리에 적용할 수 있을까? 

해시 테이블은 **해시**라는 트릭에서 이름을 얻었는데, 이를 통해 임의의 키를 정규 배열의 인덱스로 사용할 수 있는 정수로 변환할 수 있다. 따라서 숫자 인덱스로 값을 검색하는 대신 성능 저하 없이 임의 키로 검색할 수 있다. 어때요, 깔끔한가요?

실제로 해싱은 모든 키에서 작동하지는 않지만 Python에 내장된 대부분의 타입은 해싱 가능하다. 몇 가지 규칙을 따르면 [해시 가능](https://docs.python.org/3/glossary.html#term-hashable)한 타입도 만들 수 있다. 해시에 대한 자세한 내용은 다음 장을 참조하세오.
