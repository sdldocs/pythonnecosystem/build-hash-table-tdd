반세기 이상 전에 발명된 **해시 테이블**은 프로그래밍의 기본이 된 고전적인 [자료 구조](https://en.wikipedia.org/wiki/Data_structure)이다. 오늘날까지 데이터베이스 테이블 인덱싱, 계산 값 캐싱 또는 집합 구현과 같은 많은 실제 문제를 해결하는 데 도움을 준다. [취업 면접](https://realpython.com/learning-paths/python-interview/)에서 자주 물어보는 주제이며, Python에서는 해시 테이블을 곳곳에 사용하여 이름 검색을 거의 즉시 수행한다.

Python의 `dict`라는 자체 해시 테이블을 제공하지만, 해시 테이블이 배후에서 어떻게 작동하는지 이해하는 데에 도움이 될 수 있다. 코딩 평가에서 조차 이ㄹ를 구현하라고 하기도 한다. 이 튜토리얼은 마치 Python에 해시 테이블이 없는 것처럼 처음부터 구현하는 단계를 안내한다. 그 과정에서 중요한 개념을 소개하고 해시 테이블이 왜 그렇게 빠른지에 대한 이유를 제공하는 몇 가지 도전 과제을 만나게 될 것이다.

이 외에도 **TDD(Test-Driven Development)** 속성 과정을 공부하고 단계별로 해시 테이블을 구축하면서 적극적으로 실천할 예정이다 (**편역자는 여기에 관심이 있음**). TDD를 미리 경험할 필요는 없지만, 그와 동시에 경험하더라도 지루하지 않을 것이다!

이 튜토리얼에서는 다음에 대해 공부한다.

- **해시 테이블**과 **dictionary**의 차이점
- Python에서 처음부터 **해시 테이블을 구현**하는 방법
- **해시 충돌**과 기타 문제에 대처하는 방법
- **해시 함수**의 바람직한 속성
- **Python의 `hash()`**가 배후에서 작동하는 방식

이미 [Python dictionary](https://realpython.com/python-dicts/)에 익숙하고 [객체 지향 프로그래밍](https://sdldocs.gitlab.io/pythonnecosystem/oopinpython3/) 원리에 대한 기본 지식이 있다면 도움이 될 것이다. 이 튜토리얼에서 구현된 해시 테이블의 전체 소스 코드와 중간 단계를 가져오려면 [여기 링크](https://realpython.com/bonus/python-hash-table-source-code/)를 클릭하세요.

### 목차

- [Python으로 TDD방법을 사용한 해시 테이블 구축](index.md)
- [해시 테이블 데이터 구조](hash-table-data-structure.md)
    - [해시 테이블과 사전](hash-table-data-structure.md#hash-table-vs-dictionary)
    - [해시 테이블: 해시 함수가 있는 배열](hash-table-data-structure.md#hash-table)
- [해시 함수의 이해](understanding-hash-function.md)
    - [Python의 내장 hash() 검토](understanding-hash-function.md#python-built-in-hash)
    - [Python `hash()`에 대해 자세히 알아보기](understanding-hash-function.md#deep-dive-hash)
    - [해시 함수 속성](understanding-hash-function.md#identify-hash-function-properties)
    - [객체의 ID를 해시와 비교](understanding-hash-function.md#compare-object-identity-hash)
    - [자신만의 해시 함수 만들기](understanding-hash-function.md#make-your-own-hash)
- [TTD를 시용하여 해시 테이블 프로타입 구축](build-hash-table-prototype-tdd.md)
    - [테스트 주도 개발 집중 과정](build-hash-table-prototype-tdd.md#crash-courese-tdd)
    - [사용자 정의 해시 테이블 정의](build-hash-table-prototype-tdd.md#define-custom-hash-table)
    - [키-값 쌍 삽입](build-hash-table-prototype-tdd.md#insert-key-value-pair)
    - [키로 값 검색](build-hash-table-prototype-tdd.md#find-value-by-key)
    - [키-값 쌍 삭제](build-hash-table-prototype-tdd.md#delete-key-value-pair)
    - [키로 값 검색](build-hash-table-prototype-tdd.md#find-value-by-key)
    - [저장된 쌍의 값 갱신](build-hash-table-prototype-tdd.md#update-value)
    - [키-값 쌍 가져오기](build-hash-table-prototype-tdd.md#get-key-value-pairs)
    - [방어 복사(Defensive Copying) 이용](build-hash-table-prototype-tdd.md#use-defensive-copying)
    - [키와 값 가져오기](build-hash-table-prototype-tdd.md#get-keys-and-values)
    - [해시 테비을의 크기](build-hash-table-prototype-tdd.md#report-hashtable-length)
    - [해시 테이블을 iterable로 만들기](build-hash-table-prototype-tdd.md#make-hash-table-iterable)
    - [해시 테이블을 텍스트로 표현](build-hash-table-prototype-tdd.md#represent-hashtable-text)
    - [해시 테이블의 동치성 테스트](build-hash-table-prototype-tdd.md#test-equality-hashtables)
- [해시 코드 콜리젼 해결](resolve-collision.md)
    - [Linear Probing으로 충돌 키 찾기](resolve-collision.md#liner-probing)
    - [`HashTable` 클래스에서 Linear Probing 사용](resolve-collision.md#use-liner-probing)
    - [해시 테이블 크기 자동 조정](resolve-collision.md#hash-table-resize)
    - [부하율 계산](resolve-collision.md#load-factor)
    - [구분된 체인으로 충돌 키 격리](resolve-collision.md#seperate-chaining)
- [해시 테이블에서 삽입 순서 유지](retain-insertion-order.md)
- [해시 가능한 키 사용](use-hashable-key.md)
    - [해시 가능성과 불변성](use-hashable-key.md#hashability-vs-immutability)
    - [Hash-Equal 계약](use-hashable-key.md#hash-equal-contract)
- [결론](conclusion.md)
