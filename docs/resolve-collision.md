이 장에서는 해시 코드 충돌을 처리하기 위한 가장 일반적인 두 가지 전략을 살펴보고 해시 테이블 클래스에 이를 구현한다. 아래의 예를 따르고 싶다면 `PYTHONHASHSEED` 변수를 0으로 설정하여 Python의 해시 랜덤화를 비활성화하는 것을 잊지 마세오.

지금쯤이면 해시 충돌이 무엇인지에 대해 이미 잘 알고 있을 것이다. 두 키가 동일한 해시 코드를 생성하여 해시 테이블의 동일한 인덱스에서 고유한 값이 서로 충돌하는 경우이다. 예를 들어, 100개의 슬롯이 있는 해시 테이블에서 해시 랜덤화를 사용하지 않을 때 키 "easy"와 키 "difficult"는 동일한 인덱스를 갖는다.

```python
>>> hash("easy") % 100
43

>>> hash("difficult") % 100
43
```

현재 해시 테이블에서 동일한 슬롯에 다른 값을 저장하려는 시도를 알 수 없다.

```python
>>> from hashtable import HashTable

>>> hash_table = HashTable(capacity=100)
>>> hash_table["easy"] = "Requires little effort"
>>> hash_table["difficult"] = "Needs much skill"

>>> print(hash_table)
{'difficult': 'Needs much skill'}

>>> hash_table["easy"]
'Needs much skill'
```

"easy" 키로 식별된 쌍을 덮어쓰게 될 뿐만 아니라, 더 나쁜 것은 현재 존재하지 않는 키의 값을 검색하면 잘못된 값을 얻게 된다!

해시 충돌을 해결하기 위해 사용할 수 있는 세 가지 전략은 다음과 같다.

| **방법** | **설명** |
|---------|---------|
| Perfect Hashing | 처음부터 해시 충돌을 피하기 위해 [perfect hash function](https://en.wikipedia.org/wiki/Perfect_hash_function)를 선택 |
| Open Addressing | 충돌한 값을 나중에 검색할 수 있도록 예측 가능한 방식으로 분산 |
| Closed Addressing | 충돌한 값을 검색할 수 있도록 별도의 데이터 구조에 보관 |

Perfect Hashing은 모든 값을 미리 알고 있을 때만 가능하지만, 다른 두 [해시 충돌 해결](https://en.wikipedia.org/wiki/Hash_table#Collision_resolution) 방법이 더 실용적이므로 이 튜토리얼에서 자세히 살펴볼 것이다. **open addressing**은 다음과 같은 특정 알고리즘으로 다시 나눌 수 있다.

- [Cuckoo hashing](https://en.wikipedia.org/wiki/Cuckoo_hashing)
- [Double hashing](https://en.wikipedia.org/wiki/Double_hashing)
- [Hopscotch hashing](https://en.wikipedia.org/wiki/Hopscotch_hashing)
- [Linear probing](https://en.wikipedia.org/wiki/Linear_probing)
- [Quadratic probing](https://en.wikipedia.org/wiki/Quadratic_probing)
- [Robin Hood hashing](https://en.wikipedia.org/wiki/Hash_table#Robin_Hood_hashing)

대조적으로, **closed addressing**은 [seperate chaining](https://en.wikipedia.org/wiki/Hash_table#Separate_chaining)으로 잘 알려져 있다. 또한, open 및 closed addressing 배경 아이디어를 하나의 알고리즘으로 결합한 [coalesced hasing](https://en.wikipedia.org/wiki/Coalesced_hashing)도 있다.

테스트 중심 개발을 따르려면 먼저 테스트 케이스를 설계해야 한다. 하지만 해시 충돌을 어떻게 테스트할까? Python의 내장 함수는 기본적으로 일부 데이터 타입에 대해 해시 랜덤화를 사용하므로 동작을 예측하기가 매우 어렵다. `PYTHONHASHSEED` 환경 변수를 사용하여 해시 시드를 수동으로 선택하는 것은 비현실적이며 테스트 케이스를 취약하게 만들 수 있다.

이 문제를 해결하는 가장 좋은 방법은 Python의 unitest.mock과 같은 [mocking library](https://realpython.com/python-mock-library/)를 사용하는 것이다.

```python
from unittest.mock import patch

@patch("builtins.hash", return_value=42)
def test_should_detect_hash_collision(hash_mock):
    assert hash("foobar") == 42
```

[patching](https://realpython.com/python-mock-library/#patch)은 일시적으로 한 객체를 다른 객체로 바꾼다. 예를 들어, 내장 `hash()` 함수를 항상 동일한 예상 값을 반환하는 가짜 함수로 대체하여 테스트를 반복할 수 있다. 이 치환은 함수 호출 중에만 영향을 미치며, 그 후 원래의 `hash()`가 다시 돌아온다.

전체 테스트 함수에 대해 `@patch` decorator를 적용하거나 컨텍스트 관리자를 사용하여 모의 객체(mock object)의 범위를 제한할 수 있다.

```python
from unittest.mock import patch

def test_should_detect_hash_collision():
    assert hash("foobar") not in [1, 2, 3]
    with patch("builtins.hash", side_effect=[1, 2, 3]):
        assert hash("foobar") == 1
        assert hash("foobar") == 2
        assert hash("foobar") == 3
```

컨텍스트 관리자를 사용하면 동일한 테스트 케이스 내에서 내장 `hash()` 함수와 모킹된 버전에 액세스할 수 있다. 원한다면 심지어 mocked 함수는 여러 결과를 가질 수도 있다. `side_effect` 매개 변수를 사용하면 모의(mocked) 객체가 연속 호출 시 반환할 예외 또는 일련의 값을 지정할 수 있다.

이 튜토리얼의 나머지 부분에서는 테스트 기반 개발을 엄격하게 따르지 않고 `HashTable` 클래스에 더 많은 기능을 계속 추가한다. 클래스를 수정하면 기존 테스트 중 일부가 실패할 수도 있지만 간결성을 위해 새 테스트를 생략한다. 그러나 다운로드할 수 있는 첨부 자료에서 동작하는 테스트 수트을 찾을 수 있다.

## <a name='liner-probing'>Linear Probing으로 충돌 키 찾기</a>
해시 코드 충돌의 배후에 있는 이론을 이해하기 위해 잠시 둘러 갑시다. 이를 다루는 데 있어 [선형 탐색(linear probing)](https://en.wikipedia.org/wiki/Linear_probing)은 가장 오래되고, 가장 간단하고, 가장 효과적인 기술 중 하나이다. 여기에는 **삽입**, **조회**, **삭제** 및 **업데이트** 작업에 몇 가지 추가 단계가 필요하다.

일반적인 약어를 사용하여 [Python 용어집](https://docs.python.org/3/glossary.html)을 나타내는 샘플 해시 테이블을 생각해 봅시다. 총 10개의 슬롯을 수용할 수 있지만, 그 중 4개는 이미 다음과 같은 key-value 쌍에 의해 사용되고 있다.

| **Index** | **Key** | **Value** |
|:---------:|:--------|:----------|
| 0 | | |
| 1 | BDFL | Benevolent Dictator For Life |
| 2 | | |
| 3 | REPL | Read–Evaluate–Print Loop |
| 4 | | |
| 5 | | |
| 6 | | |
| 7 | | |
| 8 | PEP | Python Enhancement Proposal |
| 9 | WSGI | Web Server Gateway Interface |

이제 [method resolution order](https://docs.python.org/3/glossary.html#term-method-resolution-order)의 약자인 **MRO**를 정의하기 위해 용어집에 이를 넣으려고 한다. 키의 해시 코드를 계산하고 모듈로(modulo) 연산자를 사용하여 잘라내면 0과 9 사이의 인덱스를 얻을 수 있다.

```python
>>> hash("MRO")
8199632715222240545

>>> hash("MRO") % 10
5
```

모듈로 연산자를 사용하는 대신 적절한 [비트마스크(bitmask)](https://realpython.com/python-bitwise-operators/#bitmasks)로 해시 코드를 잘라낼 수 있는데, 이는 Python의 `dict`에서 내부적으로 작동하는 방식이다.

> **Note:** 일관된 해시 코드를 얻으려면 `PYTHONHASHSEED` 환경 변수를 0으로 설정하여 해시 랜덤화(hash randomization)를 사용하지 않도록 설정한다.

좋아요! 해시 테이블의 인덱스 5에 새 key-value 쌍을 삽입할 수 있는 빈 슬롯이 있다.

| **Index** | **Key** | **Value** |
|:---------:|:--------|:----------|
| 0 | | |
| 1 | BDFL | Benevolent Dictator For Life |
| 2 | | |
| 3 | REPL | Read–Evaluate–Print Loop |
| 4 | | |
| 5 | <span style ="background:yellow">MRO</span> | <span style ="background:yellow">Method Resolution Order</span> |
| 6 | | |
| 7 | | |
| 8 | PEP | Python Enhancement Proposal |
| 9 | WSGI | Web Server Gateway Interface |

지금까지는 문제 없다. 해시 테이블은 여전히 50%의 사용 가능한 공간있으므로 [EAFP](https://docs.python.org/3/glossary.html#term-EAFP) 약어를 삽입할 때까지 계속해서 용어를 추가할 수 있다. EAFP의 해시 코드는 [BDFL](https://docs.python.org/3/glossary.html#term-BDFL) 용어와 같이 슬롯 인덱스가 1로 잘라진다.

```python
>>> hash("EAFP")
-159847004290706279

>>> hash("BDFL")
-6396413444439073719

>>> hash("EAFP") % 10
1

>>> hash("BDFL") % 10
1
```

두 개의 서로 다른 키를 충돌시키는 해시 코드의 가능성은 상대적으로 작다. 그러나 이러한 해시 코드를 작은 범위의 배열 인덱스에 투영하는 것은 또 다른 이야기이다. 선형 탐색을 사용하면 충돌한 key-value 쌍을 서로 옆에 저장하여 이러한 충돌을 감지하고 완화할 수 있다.

| **Index** | **Key** | **Value** |
|:---------:|:--------|:----------|
| 0 | | |
| 1 | BDFL | Benevolent Dictator For Life |
| 2 | <span style ="background:yellow">EAFP</span> | <span style ="background:yellow">Easier To Ask For Forgiveness Than Permission</span> |
| 3 | REPL | Read–Evaluate–Print Loop |
| 4 | | |
| 5 | MRO | Method Resolution Order |
| 6 | | |
| 7 | | |
| 8 | PEP | Python Enhancement Proposal |
| 9 | WSGI | Web Server Gateway Interface |

BDFL과 EAFP 키가 동일하게 인덱스 1을 제공하지만, 처음 삽입된 key-value 쌍만 사용하게 된다. 두 번째 쌍은 점유된 인덱스 옆에 배치된다. 따라서 선형 탐색은 해시 테이블을 삽입 순서에 민감하게 된다.

> **Note:** 선형 탐색 또는 다른 해시 충돌 해결 방법을 사용할 경우 해당 슬롯을 찾기 위해 해시 코드에만 의존할 수 없다. 또한 키를 비교해야 한다.

[추상 기본 클래스(abstract base classes)](https://docs.python.org/3/glossary.html#term-abstract-base-class)의 약어 ABC를 추가하는 것을 고려해 봅시다. ABC의 해시 코드를 구한 다음, 이로부터 구한 슬롯 인덱스에는 PEP가 이미 점유하고 있기 때문에 다음 슬롯에 저장하여야 하나, 그러나 인덱스의 다음 위치도 삽입할 수 없다. 일반적인 상황에서는 사용 가능한 슬롯을 계속 찾지만 마지막 인덱스에 도달했기 때문에 인덱스 0에 새 약어 ABC를 삽입해야 한다.

| **Index** | **Key** | **Value** |
|:---------:|:--------|:----------|
| 0 | <span style ="background:yellow">ABC</span> | <span style ="background:yellow">Abstract Base Class</span> |
| 1 | BDFL | Benevolent Dictator For Life |
| 2 | EAFP | Easier To Ask For Forgiveness Than Permission |
| 3 | REPL | Read–Evaluate–Print Loop |
| 4 | | |
| 5 | MRO | Method Resolution Order |
| 6 | | |
| 7 | | |
| 8 | PEP | Python Enhancement Proposal |
| 9 | WSGI | Web Server Gateway Interface |

이와 같이 해시 테이블에서 저장되어 있는 key-value 쌍을 **검색**하려면 먼저 예상 인덱스에서 부터 시작한다. 예를 들어, ABC 키의 값을 찾으려면 해시 코드를 계산하고 인덱스에 매핑한다.

```python
>>> hash("ABC")
-4164790459813301872

>>> hash("ABC") % 10
8
```
인덱스 8에 저장된 key-value 쌍이 있지만 다른 키 PEP를 가지고 있으므로 인덱스를 늘려서 건너뛴다. 다음 해당 슬롯은 관련이 없는 용어인 WSGI에 의해 점유되어 있으므로, 계속하여 마침내 색인 0에서 일치하는 키를 가진 쌍을 찾을 수 있다. 그것이 해답이다.

일반적으로 검색 작업에는 가능한 세가지 중지 조건이 있다.

1. 일치하는 키를 찾았다.
2. 일치하는 키를 찾지 못하고 모든 슬롯을 방문했다.
3. 빈 슬롯을 찾았는데, 이는 검색 키가 없음을 나타낸다.

마지막 3은 저장된 key-value 쌍의 **삭제**를 더 까다롭게 만든다. 해시 테이블에서 항목을 제거한 경우 이전 충돌과 상관없이 빈 슬롯이 추가되면 검색이 중지된다. 충돌한 key-value 쌍에 다시 연결할 수 있도록 하려면 해당 key-value 쌍을 다시 사용하거나 [지체 삭제(lazy deletion)](https://en.wikipedia.org/wiki/Lazy_deletion) 전략을 사용해야 한다.

후자는 구현하기가 덜 어렵지만 필요한 검색 단계의 수를 증가시키는 추가 비용이 발생한다. 기본적으로 key-value 쌍을 삭제하는 대신 아래의 빨간색 X(❌)로 표시된 센티넬(sentinel) 값으로 대체하면 이전에 충돌한 항목을 찾을 수 있다. BDFL과 PEP 용어를 삭제하고 싶다고 하자.

| **Index** | **Key** | **Value** |
|:---------:|:--------|:----------|
| 0 | ABC | Abstract Base Class |
| 1 | ❌ | |
| 2 | EAFP | Easier To Ask For Forgiveness Than Permission |
| 3 | REPL | Read–Evaluate–Print Loop |
| 4 | | |
| 5 | MRO | Method Resolution Order |
| 6 | | |
| 7 | | |
| 8 | ❌ | |
| 9 | WSGI | Web Server Gateway Interface |

해당 key-value 쌍을 센티넬 값의 두 인스턴스로 대체했다. 나중에 ABC 키를 찾을 때, 예를 들어 인덱스 8에서 못 찾고 다음 WSGI으로 계속 이동하고, 일치하는 키를 사용하여 인덱스 0에 도달할 수 있다. 센티넬 중 하나라도 없다면, 우리는 훨씬 더 일찍 검색을 중단할 것이고, 그런 키가 없다는 결론을 내릴 수 있을 것이다.

> **Note:** 새 key-value 쌍을 삽입할 때 센티넬에 자유롭게 덮어쓸 수 있으므로 해시 테이블의 용량은 영향을 받지 않는다. 반면에, 만약 해시 테이블이 채워져 있고 대부분의 요소를 삭제했다면, 여러분은 사실상 [선형 탐색](https://realpython.com/binary-search-python/#linear-search) 알고리즘으로 끝날 수 있을 것이다.

지금까지 삽입, 삭제 및 조회에 대해 알아보았다. 그러나 선형 탐색을 사용하여 해시 테이블의 기존 항목 값을 업데이트하는 것에 대한 한 가지 단점이 있다. 업데이트할 쌍을 검색할 때 슬롯이 다른 키를 가진 다른 쌍에 의해 사용되거나 센티넬 값이 포함된 경우에만 슬롯을 건너뛴다. 반면, 슬롯이 비어 있거나 일치하는 키가 있으면 새 값을 설정해야 한다.

다음 장에서는 해시 충돌 해결을 위해 선형 탐색을 사용하도록 해시 테이블 클래스를 수정한다.

## <a name='use-liner-probing'>`HashTable` 클래스에서 Linear Probing 사용</a>
선형 탐색 이론으로 잠시 우회하였다. 이제 코딩으로 돌아갑시다. 선형 탐색은 해시 테이블의 네 가지 기본 [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) 작업에 모두 사용되므로 클래스에 도우미 메서드를 작성하여 해시 테이블의 슬롯을 방문하는 논리를 캡슐화하는 데 도움이 될 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def _probe(self, key):
        index = self._index(key)
        for _ in range(self.capacity):
            yield index, self._slots[index]
            index = (index + 1) % self.capacity
```

키가 지정되면 해당 해시 코드를 사용하여 해시 테이블에서 예상되는 인덱스를 찾는 것으로 시작한다. 그런 다음 계산된 인덱스부터 시작하여 해시 테이블의 사용 가능한 모든 슬롯 검색을 반복합니다. 각 단계에서 현재 인덱스와 관련 쌍을 반환한다. 이 쌍이 비어 있거나 삭제된 것으로 표시될 수 있다. 그런 다음 인덱스를 늘리고 필요한 경우 처음으로 돌아간다.

그런 다음 `.__setitem__()` 메서드를 다시 작성할 수 있다. 새로운 센티넬에 대한 필요성을 잊지 마세요. 이 값을 사용하면 사용된 적이 없는 슬롯과 충돌한 적이 있지만 현재는 삭제된 슬롯을 구분할 수 있다.

```python
# hashtable.py

DELETED = object()

# ...

class HashTable:
    # ...

    def __setitem__(self, key, value):
        for index, pair in self._probe(key):
            if pair is DELETED: continue
            if pair is None or pair.key == key:
                self._slots[index] = Pair(key, value)
                break
        else:
            raise MemoryError("Not enough capacity")
```

슬롯이 비어 있거나 일치하는 키를 가진 쌍이 포함되어 있으면 현재 인덱스에 새 key-value 쌍을 재할당하고 선형 탐색을 중지한다. 그렇지 않으면 다른 쌍이 해당 슬롯을 차지하고 다른 키를 가지고 있거나 슬롯이 삭제된 것으로 표시되어 있으면 사용 가능한 슬롯을 찾을 때까지 옆으로 이동한다. 사용 가능한 슬롯이 부족한 경우 해시 테이블의 용량이 부족함을 나타내기 위해 메모리 오류 예외를 발생시킨다.

> **Note:** 공교롭게도 `.__setitem__()` 메서드는 기존 쌍의 값 업데이트도 포함한다. 쌍은 불변 튜플로 표시되므로 값 구성 요소뿐만 아니라 전체 쌍을 일치하는 키로 대체합니다.

선형 탐색을 사용하여 해시 테이블에서 key-value 쌍을 가져오거나 삭제하는 작업은 유사하게 수행된다.

```python
# hashtable.py

DELETED = object()

# ...

class HashTable:
    # ...

    def __getitem__(self, key):
        for _, pair in self._probe(key):
            if pair is None:
                raise KeyError(key)
            if pair is DELETED:
                continue
            if pair.key == key:
                return pair.value
        raise KeyError(key)

    def __delitem__(self, key):
        for index, pair in self._probe(key):
            if pair is None:
                raise KeyError(key)
            if pair is DELETED:
                continue
            if pair.key == key:
                self._slots[index] = DELETED
                break
        else:
            raise KeyError(key)
```

유일한 차이점은 `return pair.value`과 `self._slots[index] = DELETED` 라인에 있다. 쌍을 삭제하려면 해시 테이블의 위치를 알아야 센티넬 값으로 대체할 수 있다. 반면 키로 검색할 때만 해당 값에 관심이 있다. 이 코드 중복이 문제가 된다면 연습으로 리팩터링을 시도해 볼 수 있다. 하지만, 그것을 명시적으로 작성하는 것이 요점에 도움이 될 수 있다.

> **Note:** 이것은 등치성 테스트 연산자(`==`)를 사용하여 키를 비교하여 요소를 탐색하는 해시 테이블의 교과서적인 구현이다. 그러나 실제 구현에서는 해시 코드를 키 및 값과 함께 쌍이 아닌 삼중으로 저장함으로써 잠재적으로 비용이 많이 드는 작업이다. 반면에 해시 코드는 비교하기에 저렴하다.
> 
> [해시 등치 계약(hash-eual contract)](https://realpython.com/python-hash-table/#the-hash-equal-contract)을 활용하여 작업 속도를 높일 수 있다. 두 해시 코드가 서로 다른 경우, 서로 다른 키에서 비롯된다는 것이 보장되므로 비용이 많이 드는 등치성 테스트를 수행할 필요가 없다. 이 방법으로 비교 횟수를 크게 줄일 수 있다.

주목해야 할 또 다른 중요한 세부 사항이 있다. 해시 테이블의 슬롯은 더 이상 두 개의 상태(빈 상태 또는 사용 상태) 중 하나일 수 밖에 없다. 센티넬 값을 해시 테이블에 삽입하여 슬롯이 삭제됨으로 표시하면 해시 테이블의 `.pairs`, `.key` 및 `.values` 속성이 엉망이 되고 길이가 정확하지 않을 수 있다. 이 문제를 해결하려면 key-value 쌍을 반환할 때 None 값과 DELETED 값을 모두 필터링해야 한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def pairs(self):
        return {
            pair for pair in self._slots
            if pair not in (None, DELETED)
        }
```

이 작은 업데이트를 통해 해시 테이블은 충돌한 쌍을 분산시키고 선형 방식으로 찾음으로써 해시 충돌을 처리할 수 있다.

```python
>>> from unittest.mock import patch
>>> from hashtable import HashTable

>>> with patch("builtins.hash", return_value=24):
...     hash_table = HashTable(capacity=100)
...     hash_table["easy"] = "Requires little effort"
...     hash_table["difficult"] = "Needs much skill"

>>> hash_table._slots[24]
Pair(key='easy', value='Requires little effort')

>>> hash_table._slots[25]
Pair(key='difficult', value='Needs much skill')
```

충돌한 두 키 "`easy`"와 "`difficult`"는 동일한 해시 코드 24임에도 불구하고 `._slots` 목록에서 서로 옆에 나타난다. 해시 테이블에 추가한 순서와 동일하게 나열된다. 삽입 순서를 바꾸거나 해시 테이블의 용량을 변경하고 슬롯에 어떤 영향을 미치는지 확인하도록 하자.

현재 해시 테이블의 용량은 고정되어 있다. 선형 탐색을 구현하기 전에 인식하지 못하고 충돌한 값을 계속 삽입하였다. 이제 해시 테이블에 더 이상 공간이 없는 경우를 감지하여 해당 예외를 발생시킬 수 있다. 그러나 해시 테이블이 필요에 따라 동적으로 용량을 확장하도록 하는 것이 더 바람직 하지 않을까?

## <a name='hash-table-resize'>해시 테이블 크기 자동 조정</a>
해시 테이블 크기를 조정하는 두 가지 다른 전략이 있다. 마지막 순간까지 기다렸다가 해시 테이블이 가득 찰 때만 크기를 조정하거나 특정 임계값에 도달한 후 먼저 조정할 수도 있다. 양쪽 모두 장단점이 있다. **지연 전략(lazy strategy)**은 거의 틀림없이 실행하기가 더 간단하기 때문에 먼저 자세히 살펴볼 것이다. 하지만, 더 많은 충돌과 더 나쁜 성능으로 이어질 수 있다.

해시 테이블의 슬롯 수를 늘려야 하는 유일한 경우는 새로운 쌍을 삽입하지 못해 `MemoryError` 예외가 발생할 때이다. `raise` 문을 다른 helper 메서드를 만들어 그 호출로 대체한 다음 대괄호 구문을 통해 `.__setitem__()`를 재귀적으로 호출한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __setitem__(self, key, value):
        for index, pair in self._probe(key):
            if pair is DELETED: continue
            if pair is None or pair.key == key:
                self._slots[index] = Pair(key, value)
                break
        else:
            self._resize_and_rehash()
            self[key] = value
```

모든 슬롯이 합법적인 쌍 또는 센티넬 값에 의해 사용된 것으로 확인되면 더 많은 메모리를 할당하고 기존 쌍을 복사한 다음 새 key-value 쌍을 다시 삽입해야 한다.

> **Note:** 이전 key-value 쌍을 더 큰 해시 테이블에 넣으면 완전히 다른 슬롯으로 해시된다. 재해싱은 방금 생성한 추가 슬롯을 활용하므로 새 해시 테이블의 충돌 수는 줄어들 것이다. 또한 센티넬 값으로 삭제 표시되었던 슬롯을 회수하여 공간을 절약할 수 있다. key-value 쌍이 새 슬롯을 찾기 때문에 과거의 충돌에 대해 걱정할 필요없다.

이제 다음과 같은 방법으로 크기 조정과 재해싱을 구현할 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def _resize_and_rehash(self):
        copy = HashTable(capacity=self.capacity * 2)
        for key, value in self.pairs:
            copy[key] = value
        self._slots = copy._slots
```

해시 테이블의 로컬 복사본을 만든다. 얼마나 더 많은 슬롯이 필요한지 예측하기 어렵기 때문에 어림짐작으로 용량 크기를 두 배로 늘린다. 그런 다음 기존 key-value 쌍 집합을 반복하여 복사본에 삽입한다. 마지막으로 `._slots` 속성이 확장된 슬롯 목록을 가리키도록 인스턴스에 다시 할당한다.

해시 테이블은 이제 필요할 때 동적으로 크기를 늘릴 수 있으므로 수행하여 보자. 용량이 1인 빈 해시 테이블을 만들고 여기에 일부 key-vlaue 쌍을 삽입해 봅시다.

```python
>>> from hashtable import HashTable
>>> hash_table = HashTable(capacity=1)
>>> for i in range(20):
...     num_pairs = len(hash_table)
...     num_empty = hash_table.capacity - num_pairs
...     print(
...         f"{num_pairs:>2}/{hash_table.capacity:>2}",
...         ("▣" * num_pairs) + ("□" * num_empty)
...     )
...     hash_table[i] = i
...
 0/ 1 □
 1/ 1 ▣
 2/ 2 ▣▣
 3/ 4 ▣▣▣□
 4/ 4 ▣▣▣▣
 5/ 8 ▣▣▣▣▣□□□
 6/ 8 ▣▣▣▣▣▣□□
 7/ 8 ▣▣▣▣▣▣▣□
 8/ 8 ▣▣▣▣▣▣▣▣
 9/16 ▣▣▣▣▣▣▣▣▣□□□□□□□
10/16 ▣▣▣▣▣▣▣▣▣▣□□□□□□
11/16 ▣▣▣▣▣▣▣▣▣▣▣□□□□□
12/16 ▣▣▣▣▣▣▣▣▣▣▣▣□□□□
13/16 ▣▣▣▣▣▣▣▣▣▣▣▣▣□□□
14/16 ▣▣▣▣▣▣▣▣▣▣▣▣▣▣□□
15/16 ▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣□
16/16 ▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣
17/32 ▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣□□□□□□□□□□□□□□□
18/32 ▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣□□□□□□□□□□□□□□
19/32 ▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣□□□□□□□□□□□□□
```

20 key-value 쌍을 성공적으로 삽입했을 때 오류가 발생하지 않았다. 이 대략적인 설명을 통해 해시 테이블이 가득 차서 더 많은 슬롯이 필요할 때 발생하는 슬롯의 이중화를 명확히 보여주고 있다.

방금 구현한 자동 크기 조정 기능 덕분에 새 해시 테이블에 대한 기본 용량을 가정할 수 있다. 이러한 방식으로 해시 테이블 클래스의 인스턴스를 만들면 초기 용량을 지정할 필요가 없어지지만 성능이 향상될 수 있다. 초기 용량에 대한 일반적인 선택은 8과 같이 2의 거듭제곱이다.

```python
# hashtable.py

# ...

class HashTable:
    @classmethod
    def from_dict(cls, dictionary, capacity=None):
        hash_table = cls(capacity or len(dictionary))
        for key, value in dictionary.items():
            hash_table[key] = value
        return hash_table

    def __init__(self, capacity=8):
        if capacity < 1:
            raise ValueError("Capacity must be a positive number")
        self._slots = capacity * [None]

    # ...
```

이를 통해 매개 변수가 없는 `HashTable()`에 대한  초기화 호출로 해시 테이블을 만들 수 있다. 클래스 메서드 `HashTable.from_dict()`를 업데이트하여 사전의 길이를 초기 용량으로 사용할 수도 있다. 이전에는 처리되지 않은 해시 충돌로 인해 테스트를 통과하는 데 필요한 임의 요인 때문에 사전의 길이를 곱했다.

앞서 언급했듯이, 크기 조정 지연 전략에는 한 가지 문제가 있다. 그것은 충돌 가능성이 높아진다는 것이다. 다음에 그것에 대해 다룰 것이다.

## <a name='load-factor'>부하율 계산</a>
해시 테이블이 포화 상태가 될 때까지 기다리는 것은 최적의 방법일 수 없다. 우선 총 용량에 도달하기 전에 해시 테이블의 크기를 조정하여 충돌을 방지할 수 있다. 크기를 조정하고 다시 시도할 수 있는 최적의 순간을 어떻게 결정할까? 부하율(load factor)을 사용한다!

**부하율**은 삭제된 슬롯을 포함하여 현재 사용 중인 슬롯 수와 해시 테이블의 모든 슬롯의 비율이다. 부하율이 높을수록 해시 충돌 가능성이 커지므로 검색 성능이 저하된다. 따라서 부하율이 항상 상대적으로 작은 상태를 유지하려고 한다. 해시 테이블의 크기를 증가시키는 것은 부하율이 특정 임계값에 도달할 때마다 발생한다.

특정 임계값의 선택은 컴퓨터 과학에서 [공간과 시간 트레이드오프(space-time trade-off)](https://en.wikipedia.org/wiki/Space%E2%80%93time_tradeoff)의 전형적인 예이다. 더 빈번한 해시 테이블 크기 조정은 더 저렴하지만 더 많은 메모리 소비의 비용으로 더 나은 성능으로 이어진다. 반대로, 더 오래 기다리면 메모리를 절약할 수 있지만 키 검색 속도는 더 느려진다. 아래 차트는 할당된 메모리 양과 평균 충돌 수 사이의 관계를 보여주고 있다.

![Fig 4.1](./images/fig-4-1.webp)
<center>Fig. 4-1 부하율 임계치</center>
<div style="text-align: right">(source: https://realpython.com/python-hash-table/)</div>

위 차트 배경 데이터는 초기 용량이 1인 빈 해시 테이블에 100개의 요소를 삽입하여 발생하는 평균 충돌 수를 측정한다. 측정은 다양한 부하율 임계값에 대해 여러 번 반복되었으며, 이 때 해시 테이블은 용량을 두 배로 늘리는 이산 점프로 크기를 조정했다.

약 0.75에서 나타나는 두 그래프의 교차점은 메모리 양과 충돌 횟수가 가장 적은 임계값의 **스위트 스팟(sweet spot)**을 나타낸다. 높은 부하율 임계값을 사용해도 메모리가 크게 절약되지는 않지만 충돌 횟수가 기하급수적으로 증가한다. 임계값이 작을수록 성능이 향상되지만 대부분 낭비되는 메모리 비용이 높다. 여러분에게 정말로 필요한 것은 100개의 슬롯뿐이라는 것을 기억하세요!

다양한 부하율 임계값을 사용하여 실험할 수 있지만 슬롯의 60%를 사용할 때 해시 테이블의 크기를 조정하는 것이 좋다. 다음은 `HashTable` 클래스에서 부하율 계산을 구현하는 방법이다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def load_factor(self):
        occupied_or_deleted = [slot for slot in self._slots if slot]
        return len(occupied_or_deleted) / self.capacity
```

실제 슬롯을 필터링하는 것으로 시작하여 없음이 아닌 실제 슬롯을 필터링한 다음, 부하율의 정의에 따라 비율을 계산한다. comprehension expression을 사용하기로 결정한 경우 모든 센티넬 값을 카운트하는 것 또한 list comprehension이어야 한다. 이 경우 set comprehension를 사용하면 삭제된 쌍의 반복 마커를 필터링하여 하나의 인스턴스만 남으므로 부정확한 부하율이 계산된다.

그런 다음 선택적 **부하율 임계값**을 허용하도록 클래스를 수정하고 이를 사용하여 슬롯 크기를 조정하고 재해시한다.

```python
 hashtable.py

# ...

class HashTable:
    # ...

    def __init__(self, capacity=8, load_factor_threshold=0.6):
        if capacity < 1:
            raise ValueError("Capacity must be a positive number")
        if not (0 < load_factor_threshold <= 1):
            raise ValueError("Load factor must be a number between (0, 1]")
        self._slots = capacity * [None]
        self._load_factor_threshold = load_factor_threshold

    # ...

    def __setitem__(self, key, value):
        if self.load_factor >= self._load_factor_threshold:
            self._resize_and_rehash()

        for index, pair in self._probe(key):
            if pair is DELETED: continue
            if pair is None or pair.key == key:
                self._slots[index] = Pair(key, value)
                break
```

부하율 임계값은 기본적으로 0.6으로 설정되며, 이는 모든 슬롯의 60%가 사용됨을 의미한다. 부하율 임계값을 최대 값으로 설명하기 위해 엄격한 부등식(>=) 대신 약한 부등식(>))을 사용한다. 이 값은 절대로 1보다 클 수 없다. 부하율이 1이면 다른 key-value 쌍을 삽입하기 전에 해시 테이블의 크기를 조정해야 한다.

훌륭해요! 해시 테이블이 조금 더 빨라졌어요. 이상으로 이 튜토리얼의 open addressing 예제를 마치겠다. 다음으로 가장 널리 사용되는 closed addressing 기술 중 하나를 사용하여 해시 충돌을 해결하여 봅시다.

## <a name='seperate-chaining'>구분된 체인으로 충돌 키 격리</a>
[구분된 체인(seperate chaining)](https://realpython.com/python-hash-table/#build-a-hash-table-prototype-in-python-with-tdd:~:text=With%20Separate%20Chaining-,Separate%20chaining,-is%20another%20extremely)은 선형 탐색보다 더 널리 퍼져 있는 또 다른 매우 인기 있는 해시 충돌 해결 방법이다. 이 아이디어는 검색 공간을 좁히기 위해 공통 기능별로 유사한 항목을 소위 **버킷(bucket)**으로 그룹화하는 것이다. 예를 들어, 과일을 수확하여 색상으로 구분된 바구니에 모으는 것으로 상상할 수 있다.

![Fig 4.2](./images/fig-4-2.webp)
<center>Fig. 4-2 각 바구니 안의 색깔별 과일</center>
<div style="text-align: right">(source: https://realpython.com/python-hash-table/)</div>

각 바구니에는 대략 같은 색 과일들이 들어 있다. 예를 들어, 여러분이 사과를 먹고 싶을 때, 여러분은 빨간색 라벨이 붙은 바구니에서만 찾으면 된다. 이상적으로는 각 바구나에  과일이 하나만 있어 즉시 찾을 수 있어야 한다. 레이블을 해시 코드로 생각하고 충돌한 key-value 쌍을 같은 색의 과일로 생각할 수 있다.

구분된 체인을 기반으로 구축된 해시 테이블은 버킷에 대한 참조 리스트로, 일반적으로 요소의 `체인(chain)`을 형성하는 [연결 리스트(linked lists)](https://realpython.com/linked-lists-python/)로 구현된다.

![Fig 4.3](./images/fig-4-3.webp)
<center>Fig. 4-3 충돌한 Key-Value 쌍들의 체인</center>
<div style="text-align: right">(source: https://realpython.com/python-hash-table/)</div>

연결 리스트에는 충돌로 인해 키가 동일한 해시 코드를 갖는 key-value 쌍들이 있다. 키로 값을 찾을 때는 먼저 올바른 버킷을 찾은 다음 버킷을 통과하여 선형 검색을 사용하여 일치하는 키를 찾아 해당 값을 반환한다. 선형 검색은 일치하는 키를 찾을 때까지 버킷의 각 항목을 하나씩 비교하는 것이다.

> **Note:** 모든 [노드](https://en.wikipedia.org/wiki/Node_(computer_science))에는 다음 요소에 대한 참조가 포함되어 있기 때문에 연결 리스트의 요소는 메모리 오버헤드가 작다. 동시에, 이러한 메모리 레이아웃은 일반적인 배열에 비해 요소의 추가와 제거가 매우 빠르다.

구분된 체인을 사용하도록 `HashTable` 클래스를 조정하려면 `._probe()` 메서드를 제거하고 슬롯을 버킷으로 바꾸는 것부터 시작하여야 한다. 이제 각 인덱스에 없음 값이나 쌍이 있는 대신 각 인덱스가 비어 있거나 비어 있지 않은 버킷을 유지하도록 만들 수 있다. 각 버킷은 연결 리스트의 헤드가 된다.

```python
# hashtable.py

from collections import deque

# ...

class HashTable:
    # ...

    def __init__(self, capacity=8, load_factor_threshold=0.6):
        if capacity < 1:
            raise ValueError("Capacity must be a positive number")
        if not (0 < load_factor_threshold <= 1):
            raise ValueError("Load factor must be a number between (0, 1]")
        self._buckets = [deque() for _ in range(capacity)]
        self._load_factor_threshold = load_factor_threshold

    # ...

    def _resize_and_rehash(self):
        copy = HashTable(capacity=self.capacity * 2)
        for key, value in self.pairs:
            copy[key] = value
        self._buckets = copy._buckets
```

연결 리스트를 처음부터 구현하는 대신 [이중 연결 리스트(doubly-linked list)](https://realpython.com/linked-lists-python/#how-to-use-doubly-linked-lists)를 사용하는 [`collections`](https://realpython.com/python-collections-module/) 모듈에서 사용할 수 있는 Python의 이중 큐를 활용할 수 있다. 일반 리스트보다 요소를 더 효율적으로 추가와 제거할 수 있다.

`.pairs`, `.capacity` 및 `.load_factor` 속성이 슬롯 대신 버킷을 참조하도록 업데이트해야 한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def pairs(self):
        return {pair for bucket in self._buckets for pair in bucket}

    # ...

    @property
    def capacity(self):
        return len(self._buckets)

    # ...

    @property
    def load_factor(self):
        return len(self) / self.capacity
```

더 이상 삭제된 요소를 표시하는 데 센티넬 값이 필요하지 않으므로 센티넬 상수를 제거한다. 이렇게 하면 key-value 쌍과 부하율의 정의가 더 간단해진다. 각 버킷에 최대 하나의 key-value 쌍을 유지하여 해시 코드 충돌 수가 최소화인 경우 용량은 버킷 수와 같다.

> **Note:** 이와 같이 정의된 부하율은 해시 테이블에 저장된 key-vlaue 쌍의 수가 버킷 수를 초과할 때 1보다 클 수 있다.

그런데 너무 많은 충돌을 허용하면 해시 테이블이 선형 시간 [복잡도](https://en.wikipedia.org/wiki/Time_complexity)을 갖는 단일 리스트로 바뀌어 그 효과로 퇴화되고 성능이 크게 저하된다. 공격자는 가능한 한 많은 충돌을 인위적으로 만들어 이 현상을 이용할 수 있다.

구분된 체인을 사용하면 모든 기본 해시 테이블 작업은 적절한 버킷을 찾고 검색하는 것으로 요약되며, 이는 해당 메서드를 비슷하게 보일 수 있다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        bucket = self._buckets[self._index(key)]
        for index, pair in enumerate(bucket):
            if pair.key == key:
                del bucket[index]
                break
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        if self.load_factor >= self._load_factor_threshold:
            self._resize_and_rehash()

        bucket = self._buckets[self._index(key)]
        for index, pair in enumerate(bucket):
            if pair.key == key:
                bucket[index] = Pair(key, value)
                break
        else:
            bucket.append(Pair(key, value))

    def __getitem__(self, key):
        bucket = self._buckets[self._index(key)]
        for pair in bucket:
            if pair.key == key:
                return pair.value
        raise KeyError(key)
```

인스턴스 디큐 연산은 인덱스의 항목을 삭제할 때 내부 참조를 업데이트한다. 사용자 정의 연결 리스트를 사용하는 경우, 수정할 때마다 충돌하는 key-value 쌍을 수동으로 다시 연결해야 한다. 이전과 마찬가지로 기존 key-value 쌍을 업데이트하려면 key-value 쌍이 변경되지 않기 때문에 이전 key-value 쌍을 새 키로 교체해야 한다.

반복하지 않으려면 Python 3.10에서 소개된 [구조 패턴 매칭(structural pattern matching)](https://realpython.com/python310-new-features/#structural-pattern-matching)을 사용하여 위의 세 메서드을 리팩터링해 보세요. 여러분은 첨부된 자료에서 하나의 가능한 해결책을 찾을 수 있다.

좋아요, 여러분은 해시 코드 충돌에 대처하는 방법을 알게 되었고, 이제 다음 단계로 넘어갈 준비가 되었다. 다음으로 해시 테이블이 키와 값을 삽입 순서대로 반할 수 있하도록 하여 봅시다.
