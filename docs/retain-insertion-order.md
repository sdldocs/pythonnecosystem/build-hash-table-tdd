고전적인 해시 테이블 데이터 구조는 **해시**를 사용하여 키를 균일하게, 때로는 의사 무작위로 분산시키기 때문에, 키의 순서를 보장할 수 없다. 경험적으로 해시 테이블 요소를 요청할 때 해시 테이블 요소가 일관된 순서로 온다고 가정할 수 없다. 하지만 때로는 추가되는 요소에 특정한 순서를 부과하는 것이 유용할 수도 있고 심지어 필요할 수도 있다.

Python 3.6 이전까지 사전 요소에 순서를 적용하는 유일한 방법은 표준 라이브러리에서 [`OrderedDict`](https://realpython.com/python-ordereddict/) 래퍼를 사용하는 것이었다. 나중에, 내장된 `dict` 데이터 타입은 key-value 쌍의 **삽입 순서**를 유지하기 시작했다. 그럼에도 불구하고 코드를 이전 버전 또는 대체 Python 버전과 호환시키기 위해 요소 순서를 유지하지 못한다고 가정하는 것이 현명할 수 있다.

사용자 정의 `HashTable` 클래스에서 유사한 삽입 순서를 유지하도록 어떻게 복제할 수 있을까? 한 가지 방법은 키를 삽입할 때 키 시퀀스를 기억하고 해당 시퀀스를 반복하여 키, 값 및 쌍을 반환하는 것이다. 클래스의 다른 내부 필드를 선언하는 것으로 시작한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __init__(self, capacity=8, load_factor_threshold=0.6):
        if capacity < 1:
            raise ValueError("Capacity must be a positive number")
        if not (0 < load_factor_threshold <= 1):
            raise ValueError("Load factor must be a number between (0, 1]")
        self._keys = []
        self._buckets = [deque() for _ in range(capacity)]
        self._load_factor_threshold = load_factor_threshold
```

해시 테이블의 내용을 수정할 때 `keys`는 커지고 축소되는 빈 리스트이다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    def __delitem__(self, key):
        bucket = self._buckets[self._index(key)]
        for index, pair in enumerate(bucket):
            if pair.key == key:
                del bucket[index]
                self._keys.remove(key)
                break
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        if self.load_factor >= self._load_factor_threshold:
            self._resize_and_rehash()

        bucket = self._buckets[self._index(key)]
        for index, pair in enumerate(bucket):
            if pair.key == key:
                bucket[index] = Pair(key, value)
                break
        else:
            bucket.append(Pair(key, value))
            self._keys.append(key)
```

해시 테이블에 연결된 key-value 쌍이 더 이상 없을 때 키를 제거한다. 반면, 새로운 key-value 쌍을 해시 테이블에 삽입할 때만 키를 추가한다. 업데이트를 수행할 때 키를 삽입하면 동일한 키의 복사본이 여러 개 생성되므로 키를 삽입하지 않는다.

그런 다음 삽입된 키의 순서를 따르도록 세 가지 속성 `.key`, `.values` 및 `.pair`를 변경한다.

```python
# hashtable.py

# ...

class HashTable:
    # ...

    @property
    def keys(self):
        return self._keys.copy()

    @property
    def values(self):
        return [self[key] for key in self.keys]

    @property
    def pairs(self):
        return [(key, self[key]) for key in self.keys]
```

내부 구현이 유출되지 않도록 키 복사본을 반환해야 한다. 또한 이 세 가지 속성 모두 정확한 순서를 유지하기 위해 집합 대신 리스트를 반환한다. 이렇게 하면 키와 값을 [zip](https://realpython.com/python-zip-function/)하여 쌍을 만들 수 있다.

```python
>>> from hashtable import HashTable
>>> hash_table = HashTable.from_dict({
...     "hola": "hello",
...     98.6: 37,
...     False: True
... })

>>> hash_table.keys
['hola', 98.6, False]

>>> hash_table.values
['hello', 37, True]

>>> hash_table.pairs
[('hola', 'hello'), (98.6, 37), (False, True)]

>>> hash_table.pairs == list(zip(hash_table.keys, hash_table.values))
True
```

키와 값은 항상 동일한 순서로 반환된다, 예를 들어 첫 번째 키가 첫 번째 값으로 매핑되는 것이다. 이는 위의 예와 같이 zip할 수 있다는 것을 의미한다.

이제 해시 테이블에서 key-value 쌍의 삽입 순서를 유지하는 방법을 알았다. 그러나 고급 기준에 따라 정렬하려면 [내장 딕셔너리 정렬(sorting built-in dㅑctionary)](https://realpython.com/iterate-through-dictionary-python/#iterating-in-sorted-order)과 동일한 기술을 사용할 수 있다. 이 시점에서 추가로 작성할 코드는 더 이상 없다.
