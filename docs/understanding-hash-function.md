[해시 함수](https://en.wikipedia.org/wiki/Hash_function)는 데이터를 **해시 값** 또는 **해시 코드**라고 하는 고정 크기 바이트 시퀀스로 변환한다. 디지털 지문이나 **다이제스트** 역할을 할 수 있는 숫자로, 일반적으로 원본 데이터보다 훨씬 작아 무결성을 확인할 수 있다. Linux 배포판의 디스크 이미지와 같은 대용량 파일을 인터넷에서 다운로드 받는 경우 다운로드 페이지에 [MD5](https://en.wikipedia.org/wiki/MD5) 또는 [SHA-2](https://en.wikipedia.org/wiki/SHA-2) [체크섬](https://en.wikipedia.org/wiki/Checksum)이 있는 경우가 있다.

사전 문제를 해결하는 것 외에도 해시 함수는 데이터 무결성을 확인하고, **보안** 및 [**암호화**](https://en.wikipedia.org/wiki/Cryptography)를 포함하는 다른 분야에 도움을 주고 있다. 예를 들어, 일반적으로 데이터 유출 위험을 줄이기 위해 해시된 비밀번호를 데이터베이스에 저장한다. 디지털 서명에는 암호화 전에 메시지 요약을 만드는 해시가 포함된다. 블록체인 트랜잭션은 암호화 목적으로 해시 함수를 사용하는 또 다른 대표적인 예이다.

> **Note:** [암호화 해시 함수](https://en.wikipedia.org/wiki/Cryptographic_hash_function)는 몇 가지 추가 요구 사항을 충족해야 하는 특별한 타입의 해시 함수이다. 그러나 이 튜토리얼에서는 해시 테이블 데이터 구조에 사용되는 가장 기본적인 형태의 해시 함수만 다루고자 한다.

많은 **해싱 알고리즘**이 있지만 이 장에서 필요한 몇 가지 공통 특징을 모두 공유하고자 한다. 좋은 해시 함수를 정확하게 구현하는 것은 [솟수](https://en.wikipedia.org/wiki/Prime_number)를 포함하는 고급 수학에 대한 이해가 필요할 수 있는 어려운 분야이다. 다행스럽게도, 여러분은 그러한 알고리즘을 구현할 필요가 없다.

Python은 보안이 덜한 체크섬 알고리즘뿐만 아니라 잘 알려진 다양한 암호화 해시 함수를 제공하는 [hashlib](https://docs.python.org/3/library/hashlib.html) 모듈이 내장되어 있다. 이 언어는 또한 사전과 집합에서 빠른 요소 검색에 주로 사용되는 global [hash()](https://docs.python.org/3.10/library/functions.html#hash) 함수를 가지고 있다. 해시 함수의 가장 중요한 속성에 대해 먼저 알아보고 그 방법에 대해 연구할 수 있을 것이다.

## <a nmae='python-built-in-hash'>Python의 내장 hash() 검토</a>
해시 함수를 처음부터 구현하기 전에 먼저 Python의 `hash()`를 분석하여 속성을 살펴보자. 이를 통해 해시 함수를 설계할 때 어떤 문제가 관련되어 있는지 이해할 수 있다.

> **Note:** 해시 함수 선택은 해시 테이블의 성능에 큰 영향을 미칠 수 있다. 따라서 나중에 이 튜토리얼에서 사용자 정의 해시 테이블을 작성할 때 내장 `hash()` 함수를 사용할 것이다. 이 장에서 해시 함수를 구현하는 것은 연습으로만 사용된다.

먼저, 숫자와 문자열과 같은 파이썬에 내장된 몇 가지 데이터 타입 리터럴에서 `hash()`를 호출하여 함수가 어떻게 동작하는지 확인해 보자.

```python
>>> hash(3.14)
322818021289917443

>>> hash(3.14159265358979323846264338327950288419716939937510)
326490430436040707

>>> hash("Lorem")
7677195529669851635

>>> hash("Lorem ipsum dolor sit amet, consectetur adipisicing elit,"
... "sed do eiusmod tempor incididunt ut labore et dolore magna"
... "aliqua. Ut enim ad minim veniam, quis nostrud exercitation"
... "ullamco laboris nisi ut aliquip ex ea commodo consequat."
... "Duis aute irure dolor in reprehenderit in voluptate velit"
... "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint"
... "occaecat cupidatat non proident, sunt in culpa qui officia"
... "deserunt mollit anim id est laborum.")
1107552240612593693
```

결과를 보면 여러 가지 점을 관찰할 수 있다. 첫째, 내장 해시 함수는 위에 표시된 입력 중 일부에 대해 사용자에게 다른 값을 반환할 수 있다. 숫자 입력은 항상 동일한 해시 값을 생성하는 것처럼 보이지만 문자열은 그렇지 않을 가능성이 높다. 왜 그럴까? `hash()`가 비결정론적(nondetermistic) 함수인 것처럼 보일 수 있지만, 그것은 아니다!

기존 인터프리터 세션에서 동일한 인수로 `hash()`를 호출하면 동일한 결과가 계속 반환된다.

```python
>>> hash("Lorem")
7677195529669851635

>>> hash("Lorem")
7677195529669851635

>>> hash("Lorem")
7677195529669851635
```

해시 값은 **변하지 않으며** 이는 객체의 수명 동안 변하지 않기 때문이다. 그러나 Python을 종료하고 다시 시작하는 즉시 Python 호출에서 다른 해시 값을 볼 수 있다. 터미널에서 `-c` 옵션을 사용하여 [one-line script](https://en.wikipedia.org/wiki/One-liner_program)를 실행하여 이를 테스트할 수 있다.

```
$ python -c "print(hash('Lorem'))"
6182913096689556094

$ python -c "print(hash('Lorem'))"
1756821463709528809

$ python -c "print(hash('Lorem'))"
8971349716938911741
```

웹 서버에서 [해시 함수의 알려진 취약성](https://ocert.org/advisories/ocert-2011-003.html)을 이용하는 [서비스 거부(DoS)](https://en.wikipedia.org/wiki/Denial-of-service_attack) 공격에 대한 대책으로 Python이 구현한 예상 동작이다. 공격자는 취약한 해시 알고리즘을 악용하여 의도적으로 소위 해시 충돌을 만들어 서버에 과부하가 걸려 액세스할 수 없게 만들 수 있다. 대부분의 피해자들은 중단없는 온라인 서비스를 통해 돈을 벌기 때문에 랜섬(random) 공격의 전형적인 동기였다.

오늘날 Python은 문자열과 같은 일부 입력에 대해 기본적으로 **해시 랜덤화(hash randomization)**를 활성화하여 해시 값의 예측 가능성을 낮춘다. 이것은 `hash()`를 좀 더 **안전하게** 만들고 공격을 더 어렵게 만든다. 그러나 다음과 같이 [PYTONHASHSEED](https://docs.python.org/3/using/cmdline.html#envvar-PYTHONHASHSEED) 환경 변수를 통해 고정 시드 값을 설정하여 랜덤화를 비활성화할 수도 있다.

```python
$ PYTHONHASHSEED=1 python -c 'print(hash("Lorem"))'
440669153173126140

$ PYTHONHASHSEED=1 python -c 'print(hash("Lorem"))'
440669153173126140

$ PYTHONHASHSEED=1 python -c 'print(hash("Lorem"))'
440669153173126140
```

이제 각 Python 호출은 알려진 입력에 대해 동일한 해시 값을 생성한다. 이는 분산된 Python 인터프리터 클러스터 간에 데이터를 파티셔닝하거나 공유하는 데 도움이 될 수 있다. 해시 랜덤화를 비활성화할 때 발생할 수 있는 위험을 이해하고 주의하여야 한다. 전반적으로 Python의 `hash()`는 실제로 해시 함수의 가장 기본적인 특징 중 하나인 결정론적 함수이다.

또한 `hash()`는 임의의 입력을 취하기 때문에 상당히 보편적으로 보인다. 즉, 다양한 종류와 크기의 값이 필요하다. 이 함수는 길이나 크기에 상관없이 문자열과 부동 소수점 번호에 상관없이 사용할 수 있다. 실제로 다음과 같이 더 이국적인 타입의 해시 값도 계산할 수 있다.

```python
>>> hash(None)
5904497366826

>>> hash(hash)
2938107101725

>>> class Person:
...     pass
...

>>> hash(Person)
5904499092884

>>> hash(Person())
8738841746871

>>> hash(Person())
8738841586112
```

여기서, 여러분은 [Python의 None 객체](https://realpython.com/null-in-python/), `hash()` 함수 자체, 그리고 심지어 몇몇 인스턴스를 가진 사용자 정의 `Person` 클래스를 해시 함수 호출에 사용하였다. 즉, 모든 객체가 해당 해시 값을 갖는 것은 아니다. 해시 함수는 다음과 같은 몇 가지 객체 중 하나에 대해 호출하려고 하면 예외를 발생시킨다.

```python
>>> hash([1, 2, 3])
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unhashable type: 'list'
```

입력의 기본 타입에 따라 해시 값을 계산할 수 있는지 여부가 결정된다. Python에서는 리스트, 집합과 딕셔너리와 같은 내장된 가변(mutable) 타입의 인스턴스를 해시할 수 없다. 그 이유에 대한 힌트는 얻었지만, [뒤에 나오는 장](use-hashable-key.md)에서 더 자세히 알아볼 것이다. 지금은 대부분의 데이터 타입이 일반적으로 해시 함수에 작동한다고 가정하자.

## <a name='deep-dive-hash'>Python `hash()`에 대해 자세히 알아보기</a>
`hash()`의 또 다른 흥미로운 특징은 입력이 아무리 크다 하더라도 항상 크기가 고정된 출력을 생성한다는 것이다. Python에서 해시 값은 중간 크기의 정수이다. 때로는 음수일 수도 있으므로, 어떤 방식으로든 해시 값에 의존할 계획이라면 다음을 고려하시오.

```python
>>> hash("Lorem")
-972535290375435184
```

고정 크기 출력의 자연스러운 결과는 대부분의 원본 정보가 프로세스 중에 되돌릴 수 없이 손실된다는 것이다. 결과 해시 값이 임의로 큰 데이터의 다이제스트 역할을 하기를 원하기 때문에 문제는 없다. 그러나 해시 함수가 무한한 값 집합을 유한 공간으로 매핑하기 때문에, 두 개의 서로 다른 입력이 동일한 해시 값을 생성할 때 해시 충돌이 발생할 수 있다.

> **Note:** 만약 여러분이 수학적 성향이 있다면, 여러분은 [pigeonhole principle](https://en.wikipedia.org/wiki/Pigeonhole_principle)를 사용하여 해시 충돌을 더 공식적으로 설명할 수 있다.

>> *m*개의 항목과 *n*개의 컨테이너가 주어졌을 때, *m* > *n*인 경우, 둘 이상의 항목을 가진 컨테이너가 적어도 하나 있다.

> 이 컨텍스트에서 항목은 해시 함수에 입력하는 잠재적으로 무한 갯수이며 컨테이너는 유한 풀에 할당되는 해시 값이다.

해시 충돌은 해시 테이블에서 필수적인 개념으로, 사용자 정의 해시 테이블을 구현할 때 [나중에 더 자세히 설명](resolve-collision.md)하려고 한다. 현재로서는, 여러분은 그것들이 매우 바람직하지 않다고 생각할 수 있다. 해시 충돌은 매우 비효율적인 조회로 이어질 수 있고 해커에 의해 악용될 수 있으므로 가능한 한 피해야 한다. 따라서 바람직한 해시 함수는 보안과 효율성 모두에 대해 해시 충돌 가능성을 최소화해야 한다.

실제로, 이것은 종종 해시 함수가 사용 가능한 공간에 **균일하게 분포된** 값을 할당해야 한다는 것을 의미한다. 터미널에 텍스트 히스토그램을 표시하여 Python의 `hash()`가 생성한 해시 값의 분포를 시각화할 수 있다. 다음 코드 블록을 복사하여 `hash_distribution.py` 파일에 저장한다.

```python
# hash_distribution.py

from collections import Counter

def distribute(items, num_containers, hash_function=hash):
    return Counter([hash_function(item) % num_containers for item in items])

def plot(histogram):
    for key in sorted(histogram):
        count = histogram[key]
        padding = (max(histogram.values()) - count) * " "
        print(f"{key:3} {'■' * count}{padding} ({count})")
```

[Counter](https://realpython.com/python-counter/) 인스턴스를 사용하여 제공된 항목의 해시 값 히스토그램을 편리하게 표현할 수 있다. 해시 값은 [모듈로 연산자(modulo operrator)](https://realpython.com/python-modulo-operator/)로 wrap하여 지정된 수의 컨테이너에 분산된다. 예를 들어 인쇄 가능한 ASCII 문자 100자를 사용하여 해시 값을 계산하고 분포를 표시할 수 있다.

```python
>>> from hash_distribution import plot, distribute
>>> from string import printable

>>> plot(distribute(printable, num_containers=2))
0 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ (51)
1 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■   (49)

>>> plot(distribute(printable, num_containers=5))
0 ■■■■■■■■■■■■■■■            (15)
1 ■■■■■■■■■■■■■■■■■■■■■■■■■■ (26)
2 ■■■■■■■■■■■■■■■■■■■■■■     (22)
3 ■■■■■■■■■■■■■■■■■■         (18)
4 ■■■■■■■■■■■■■■■■■■■        (19)
```

컨테이너가 두 개뿐인 경우에는 대략 50대 50의 분포를 예상해야 한다. 컨테이너를 더 추가하면 용기를 다소 고르게 채울 수 있다. 보는 바와 같이, 내장 `hash()` 함수는 상당히 우수하지만 해시 값을 균일하게 분포시키는 데는 완벽하지 않다.

이와 관련하여 해시 값의 균일한 분포는 일반적으로 **의사 랜덤(pseudo-random)**이며, 이는 암호화 해시 함수에서 특히 중요하다. 이는 잠재적인 공격자가 통계 분석을 사용하여 해시 함수의 입력과 출력 사이의 상관 관계를 예측하는 것을 방지한다. 문자열에서 한 문자를 변경하여 Python에서 해시 값에 어떤 영향을 미치는지 확인하여 본다.

```python
>>> hash("Lorem")
1090207136701886571

>>> hash("Loren")
4415277245823523757
```

문자 하나만 달라도 지금은 완전히 다른 해시 값을 갖는다. 해시 값은 가장 작은 입력 변화에도 증폭되기 때문에 종종 [눈사태 효과(avalanche effect)](https://en.wikipedia.org/wiki/Avalanche_effect)로 영향을 미친다. 그럼에도 불구하고, 해시 함수의 이러한 특징은 해시 테이블 데이터 구조를 구현하기 위해 필수적인 것은 아니다.

대부분의 경우, Python의 `hash()`는 암호화 해시 함수의 또 다른 비필수적인 특징을 보여주는데, 이는 앞서 언급한 피죤홀 원리에 기인한다. 대부분의 경우 역함수를 찾는 것이 거의 불가능하기 때문에 단방향 함수처럼 동작한다. 그러나 다음과 같은 예외는 주목할 만하다.

```python
>>> hash(42)
42
```

작은 정수의 해시 값은 그 값 자체와 동일하다. 이는 CPython이 단순성과 효율성을 위해 사용하는 세부 구현 정보이다. 결정론적인 방법으로 해시 값을 계산할 수 있는 한 실제 해시 값은 중요하지 않다.

마지막으로, Python에서 해시 값을 계산하는 것은 매우 큰 입력에서도 **빠르다**. 현대 컴퓨터는 1억 문자의 문자열을 인수로 `hash()`를 호출할 때 즉시 반환한다. 만약 그것이 그렇게 빠르지 않다면, 해시 값 계산의 추가적인 오버헤드는 애초에 해시의 이점을 상쇄할 것이다.

## <a name='identify-hash-function-properties'>해시 함수 속성</a>
Python의 `hash()`에 대해 지금까지 배운 내용을 바탕으로 이제 일반적으로 해시 함수가 요구하는 속성에 대한 결론을 도출할 수 있다. 다음은 일반 해시 함수와 암호화 해시 함수을 비교하는 기능의 요약이다. 

| **기능** | **해시 함수** | **암호화 해시 함수** |
|:--------|:-----------:|:----------------:|
| Deterministic | :heavy_check_mark: | :heavy_check_mark: |
| Universal Input | :heavy_check_mark: | :heavy_check_mark: |
| Fixed-Sized Output | :heavy_check_mark: | :heavy_check_mark: |
| Fast to Compute | :heavy_check_mark: | :heavy_check_mark: |
| Uniformly Distributed | :heavy_check_mark: | :heavy_check_mark: |
| Randomly Distributed | | :heavy_check_mark: |
| Randomized Seed | | :heavy_check_mark: |
| One-Way Function | | :heavy_check_mark: |
| Avalanche Effect | | :heavy_check_mark: |

두 해시 함수 타입의 목표는 서로 겹치기 때문에 몇 가지 공통 기능을 공유한다. 반면에, 암호화 해시 함수는 보안에 대한 추가적인 보증을 제공한다.

여러분만의 해시 함수를 만들기 전에, Python에 내장된 가장 간단한 대체 함수를 살펴볼 것이다.

## <a name='compare-object-identity-hash'>객체의 ID를 해시와 비교</a>
아마도 Python에서 상상할 수 있는 가장 간단한 해시 함수 구현 중 하나는 객체의 ID를 알려주는 내장 `id()`일 것이다. 표준 Python 인터프리터에서 ID는 정수로 표현된 객체의 메모리 주소와 같다.

```python
>>> id("Lorem")
139836146678832
```

`id()` 함수는 대부분의 바람직한 해시 함수 속성을 가지고 있다. 결국, 그것은 매우 빠르고 어떤 입력에도 작동한다. 이것은 결정론적인 방식으로 고정 크기 정수를 반환한다. 동시에 메모리 주소를 기반으로 원래 객체를 쉽게 검색할 수 없다. 메모리 주소 자체는 객체의 수명 동안 불변하며 인터프리터 실행 사이에 다소 랜덤화된다.

그렇다면 왜 Python은 해싱에 다른 함수를 사용해야 한다고 주장하는가?

먼저, `id()`의 의도가 `hash()`와 다르므로 다른 Python 배포판들은 대안적인 방법으로 ID를 구현할 수 있다. 둘째, 메모리 주소는 균일한 분포를 갖지 않고 예측도 가능하며, 이는 안전하지 않고 해싱에 매우 비효율적이다. 마지막으로, 동일한 객체는 별개의 정체성을 가지고 있더라도 일반적으로 동일한 해시 코드를 생성해야 한다.

> **Note:** 나중에, 여러분은 값의 동일성과 해당 해시 코드 사이의 [계약](https://realpython.com/python-hash-table/#the-hash-equal-contract)에 대해 더 알게 될 것이다.

그것이 사라지면, 여러분은 마침내 여러분만의 해시 함수를 만드는 것을 생각할 수 있다.

## <a name='make-your-own-hash'>자신만의 해시 함수 만들기</a>
시작부터 모든 요구 사항을 충족하는 해시 함수를 설계하는 것은 어렵다. 앞에서 언급한 바와 같이, 여러분은 다음 섹션에서 여러분의 해시 테이블 프로토타입을 만들기 위해 내장 `hash()` 함수를 사용하게 될 것이다. 그러나 해시 함수를 처음부터 구축하려고 하는 것은 그것이 어떻게 작동하는지를 배우는 좋은 방법이다. 이 섹션을 마치면 완벽함과는 거리가 먼 기본적인 해시 함수만 갖게 되겠지만 귀중한 통찰력을 얻게 될 것이다.

이 연습에서는 처음에는 데이터 타입을 하나로 제한하고 그 주변에 조잡한 해시 함수를 구현할 수 있다. 예를 들어, 문자열을 고려하여 문자열에 포함된 개별 문자의 **순서 값**을 요약할 수 있다.

```python
>>> def hash_function(text):
...     return sum(ord(character) for character in text)
```

[생성기 식(generator expression)](https://realpython.com/introduction-to-python-generators/#building-generators-with-generator-expressions)을 사용하여 텍스트를 반복한 다음 각 개별 문자를 내장된 `ord()` 함수를 사용하여 해당 [유니코드](https://realpython.com/python-encodings-guide/)를 코드 포인트로 변환하고 마지막으로 순서 값을 함께 [더](https://realpython.com/python-sum-function/)합니다. 이것을 인수로 제공된 주어진 텍스트에 대해 하나의 숫자로 반환한다.

```python
>>> hash_function("Lorem")
511

>>> hash_function("Loren")
512

>>> hash_function("Loner")
512
```

바로 이 기능에 몇 가지 문제가 있다는 것을 알게 될 것입니다. 문자열별일 뿐만 아니라 해시 코드의 분포가 좋지 않아 유사한 입력 값으로 클러스터를 형성하는 경향이 있다. 약간의 입력 변화는 관찰된 출력에 거의 영향을 미치지 않는다. 설상가상으로, 이 함수는 텍스트의 문자 순서에 둔감하게 남아 있는데, 이는 *Loren*과 *Loner*와 같은 동일한 단어의 [철자 바꾸기(anagram)](https://en.wikipedia.org/wiki/Anagram)이 해시 코드 충돌을 초래한다는 것을 의미한다.

첫 번째 문제를 해결하려면 입력을 `str()`을 호출하는 문자열로 변환해 보자. 이제 함수는 다음과 같은 모든 타입의 인수를 처리할 수 있다.

```python
>>> def hash_function(key):
...     return sum(ord(character) for character in str(key))

>>> hash_function("Lorem")
511

>>> hash_function(3.14)
198

>>> hash_function(True)
416
```

문자열, 부동 소수점 실수 또는 부울 값을 포함한 모든 데이터 타입을 인수로 `hash_function()`을 호출할 수 있다.

이 구현은 해당 문자열 표현만큼만 좋다. 일부 객체들은 위의 코드에 적합한 텍스트 표현을 갖고 있지 않을 수 있다. 특히 `.__str__()`과 `.__repr__()`를 정확하게 구현하지 못한 사용자 정의 클래스 인스턴스가 좋은 예이다. 또한 여러 데이터 타입을 더 이상 구분할 수 없다.

```python
>>> hash_function("3.14")
198

>>> hash_function(3.14)
198
```

실제로는 문자열 "3.14"와 부동소수점 실수 3.14를 서로 다른 해시 코드를 가진 별개의 객체로 취급할 수 있다. 이를 완화하는 한 가지 방법은 `str()`을 [repr()](https://docs.python.org/3/library/functions.html#repr)과 교환하는 것이다.

```python
>>> repr("3.14")
"'3.14'"

>>> repr(3.14)
'3.14'
```

그러면 해시 기능이 어느 정도 향상된다.

```python
>>> def hash_function(key):
...     return sum(ord(character) for character in repr(key))

>>> hash_function("3.14")
276

>>> hash_function(3.14)
198
```

이제 문자열을 숫자와 구별할 수 있다. *Loren*이나 *Loner*와 같은 애너그람 문제를 해결하려면 문자의 값과 텍스트 내에서의 위치를 고려하도록 해시 함수를 수정한다.

```python
>>> def hash_function(key):
...     return sum(
...         index * ord(character)
...         for index, character in enumerate(repr(key), start=1)
...     )
```

여기서는 문자의 순서 값과 해당 인덱스를 곱하여 얻은 곱의 합을 얻는다. 인덱스를 `0`이 아닌 `1`부터 열거한다. 그렇지 않으면 첫 번째 문자의 값에 0을 곱하되므로 항상 `0`이 된다.

이제 해시 함수는 상당히 보편적이며 이전만큼 충돌을 많이 일으키지 않지만 문자열이 길수록 해시 코드가 커지기 때문에 출력이 임의로 커질 수 있다. 또한 큰 입력의 경우 속도가 상당히 느릴 것이다.

```python
>>> hash_function("Tiny")
1801

>>> hash_function("This has a somewhat medium length.")
60919

>>> hash_function("This is very long and slow!" * 1_000_000)
33304504435500117
```

해시 코드를 `100`과 같은 알려진 최대 크기로 모듈로(%) 하여 무한 확장을 항상 해결할 수 있다.

```python
>>> hash_function("Tiny") % 100
1

>>> hash_function("This has a somewhat medium length.") % 100
19

>>> hash_function("This is very long and slow!" * 1_000_000) % 100
17
```

해시 코드 풀을 더 작게 선택하면 해시 코드 충돌 가능성이 증가한다. 입력 값의 수를 미리 알지 못하는 경우 가능하면 나중에 결정하는 것이 좋다. 또한 Python에서 기본적으로 지원되는 정수의 가장 높은 값을 나타내는 [sys.max size](https://docs.python.org/3/library/sys.html#sys.maxsize)와 같은 합리적인 최대값을 가정하여 해시 코드에 제한을 가할 수도 있다.

함수의 느린 속도를 잠시 무시하면 해시 함수에 또 다른 특이한 문제가 있음을 알 수 있다. 이것은 클러스터링으로 사용 가능한 모든 슬롯을 활용하지 않음으로써 해시 코드의 차선의 분배를 초래한다.

```python
>>> from hash_distribution import plot, distribute
>>> from string import printable

>>> plot(distribute(printable, 6, hash_function))
  0 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■   (31)
  1 ■■■■                              (4)
  2 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■   (31)
  4 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ (33)
  5 ■                                 (1)
```

분포가 고르지 않다. 또한 6개의 컨테이너를 사용할 수 있지만 히스토그램에서 하나가 누락되었다. 이 문제는 `repr()`에 의해 추가된 두 개의 아포스트로피가 이 예제의 거의 모든 키를 짝수 해시 값으로 만든다는 사실에 기안한다. 다음과 같은 경우 왼쪽 아포스트로피를 제거하여 이 문제를 방지할 수 있다.

```python
>>> hash_function("a"), hash_function("b"), hash_function("c")
(350, 352, 354)

>>> def hash_function(key):
...     return sum(
...         index * ord(character)
...         for index, character in enumerate(repr(key).lstrip("'"), 1)
...     )

>>> hash_function("a"), hash_function("b"), hash_function("c")
(175, 176, 177)

>>> plot(distribute(printable, 6, hash_function))
  0 ■■■■■■■■■■■■■■■■   (16)
  1 ■■■■■■■■■■■■■■■■   (16)
  2 ■■■■■■■■■■■■■■■    (15)
  3 ■■■■■■■■■■■■■■■■■■ (18)
  4 ■■■■■■■■■■■■■■■■■  (17)
  5 ■■■■■■■■■■■■■■■■■■ (18)
```

`str.lstrip()`의 호출은 스트립할 지정된 접두사로 시작하는 문자열에만 영향을 미친다.

당연히 해시 기능을 계속해서 향상시킬 수 있다. 만약 여러분이 Python에서 문자열과 바이트 시퀀스를 위한 `hash()`의 구현이 궁금하다면, 그것은 현재 [SipHash](https://en.wikipedia.org/wiki/SipHash) 알고리즘을 사용하고 있으며, 이를 사용할 수 없다면 [FNV](https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function)의 수정된 버전으로 떨어질 수 있다. Python 인터프리터가 사용하는 해시 알고리즘을 알아보려면 `sys` 모듈로 이동한다.

```python
import sys
>>> sys.hash_info.algorithm
'siphash24'
```

이 시점에서 해시 함수가 어떻게 작동해야 하는지, 해시 함수를 구현하는 과정에서 직면할 수 있는 문제를 여러분은 상당히 잘 파악할 수 있게 되었다. 다음 장에서는 해시 함수를 사용하여 해시 테이블을 작성한다. 특정 해시 알고리즘의 선택은 해시 테이블의 성능에 영향을 미친다. 그 지식을 바탕으로 앞으로는 내장 `hash()`를 자유롭게 고수할 수 있다.
