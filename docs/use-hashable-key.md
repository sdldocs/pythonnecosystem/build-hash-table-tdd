이제 해시 테이블의 모든 기능이 완전히 작동한다. 내장 `hash()` 함수를 사용하여 임의 키를 값으로 매핑할 수 있다. 해시 코드 충돌을 감지하고 해결할 수 있으며 key-value 쌍의 삽입 순서도 유지할 수 있다. 원한다면 이론적으로 Python `dict`에 사용할 수 있다. 그 결과는 성능이 떨어지고 때로는 더 장황한 구문을 제외하고는 큰 차이를 눈치채지 못할 수 있다.

**Note:** 앞서 언급했듯이 해시 테이블과 같은 데이터 구조를 직접 구현할 필요는 거의 없다. Python은 타의 추종을 불허하는 성능을 가지며, 수많은 개발자들이 현장에서 테스트한 많은 유용한 컬렉션과 함께 제공된다. 특수 데이터 구조의 경우, 자신의 라이브러리 중 하나를 만들기 전에 타사 라이브러리에 대한 PyPI를 확인해 보도록 한다. 시간을 많이 절약하고 버그 위험을 크게 줄일 수 있다.

지금까지, 우리는 python의 대부분 내장 타입들이 해시 테이블 키로 작동할 수 있다는 것을 당연하게 여겼다. 그러나 실제로 해시 테이블 구현에 사용하려면 키를 해시 가능한 타입으로만 제한하고, 그 의미을 이해해야 한다. 사용자 정의 데이터 타입을 추가하기로 결정했을 때 특히 유용하다.

## <a name='hashability-vs-immutability'>해시 가능함과 불변성</a>
Python의 대부분 원시 데이터 타입을 포함하며, 일부 데이터 타입은 해시 가능한 반면 다른 데이터 타입은 해시 가능하지 않다는 것을 앞서 설명하였다. 해시 가능성의 주요 특성은 주어진 객체의 해시 코드를 계산하는 능력이다.

```python
hash(frozenset(range(10)))
3895031357313128696

>>> hash(set(range(10)))
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    hash(set(range(10)))
TypeError: unhashable type: 'set'
```

예를 들어, Python에서 [`frozenset`](https://realpython.com/python-sets/#frozen-sets) 데이터 타입의 인스턴스는 해시 가능한 반면, 일반 집합에는 해시가 전혀 구현되지 않았다. 해시성은 특정 타입의 객체들이 **딕셔너리 키** 또는 **집합의 원소**가 될 수 있는 가에 직접적인 영향을 미친다. 왜냐하면 두 데이터 구조 모두 내부에서 `hash()`을 사용하기 때문이다.

```python
>>> hashable = frozenset(range(10))
>>> {hashable: "set of numbers"}
{frozenset({0, 1, 2, 3, 4, 5, 6, 7, 8, 9}): 'set of numbers'}
>>> {hashable}
{frozenset({0, 1, 2, 3, 4, 5, 6, 7, 8, 9})}

>>> unhashable = set(range(10))
>>> {unhashable: "set of numbers"}
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    {unhashable: "set of numbers"}
TypeError: unhashable type: 'set'
>>> {unhashable}
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    {unhashable}
TypeError: unhashable type: 'set'
```

`frozenset`의 인스턴스는 해시 가능하지만, 정확히 동일한 값을 가진 해당 집합은 해시 가능하지 않다. 딕셔너리의 값으로 해시할 수 없는 데이터 타입을 계속 사용할 수 있다. 해당 해시 코드를 계산할 수 있어야 하는 것은 딕셔너리의 키이다.

> **Note:** `frozenset`같이 해시 가능한 컨테이너에 해시할 수 없는 객체를 삽입하면 해당 컨테이너도 해시할 수 없게 된다.

해시 가능성은 객체의 수명 동안 내부 상태의 변경 가능성, 즉 **가변성(mutability)**과 밀접한 관련이 있다. 그 둘 사이의 관계는 주소를 바꾸는 것과 같다. 여러분이 다른 곳으로 이사했을 때, 여러분은 여전히 같은 사람이지만, 여러분의 오랜 친구들은 여러분을 찾는 데 어려움을 겪을 수 있다.

리스트, 집합 또는 딕셔너리와 같은 Python에서 해시할 수 없는 유형은 요소를 추가하거나 제거하여 값을 수정할 수 있으므로 **변경 가능한(mutable)** 컨테이너이다. 반면에 Python에 내장된 해시 가능한 타입은 대부분 **변경 할 수 없다(immutable)**. 이것은 변형 가능한 타입이 해시될 수 없다는 것을 의미합니까?

정답은 그것들이 변경될 수도 있고 해쉬할 수도 있지만, 그것들은 거의 해서는 안 된다는 것이다! 키를 변경하면 해시 테이블 내에서 메모리 주소가 변경된다. 사용자 정의 클래스를 예로 들어 보겠다.

```python
>>> class Person:
...     def __init__(self, name):
...         self.name = name
```

이 클래스는 이름이 있는 사람을 나타낸다. Python은 클래스내에 특별한 메서드 `.__hash_()`을 위한 기본 구현을 제공한다. 이는 해시 코드를 생성하기 위하여 단순히 객체의 ID를 사용하는 것이다.

```python
>>> hash(Person("Joe"))
8766622471691

>>> hash(Person("Joe"))
8766623682281
```

각 개별 `Person` 인스턴스는 논리적으로 다른 인스턴스와 동일한 경우에도 고유한 해시 코드를 갖는다. 객체의 값이 해시 코드를 결정하도록 하기 위하여 다음과 같이 `.__hash__()`의 기본 구현을 오버라이드할 수 있다.

```python
>>> class Person:
...     def __init__(self, name):
...         self.name = name
...
...     def __hash__(self):
...         return hash(self.name)

>>> hash(Person("Joe")) == hash(Person("Joe"))
True
```

이름이 같은 `Person` 클래스의 인스턴스가 항상 동일한 해시 코드를 갖도록 `.name` 속성을 파라메터로 `hash()`를 호출한다. 예를 들어, 이는 딕셔너리에서 그들을 찾는 데 편리하다.

> **Note:** 클래스의 .__hash__ 속성을 `None`으로 설정하여 명시적으로 해시할 수 없는 것으로 표시할 수 있다.

```python
>>> class Person:
...     __hash__ = None
...
...     def __init__(self, name):
...         self.name = name

>>> hash(Person("Alice"))
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    hash(Person("Alice"))
TypeError: unhashable type: 'Person'
```

> 이렇게 하면 `hash()`가 클래스의 인스턴스에서 작동하지 않는다.

해시 가능한 타입의 또 다른 특성은 인스턴스를 값별로 비교할 수 있다는 점이다. 해시 테이블은 키의 등치성 테스트 연산자(`==`)로 비교하므로 이를 허용하려면 클래스에 특별한 메서드인 `.__eq__()`를 구현해야 한다.

```python
>>> class Person:
...     def __init__(self, name):
...         self.name = name
...
...     def __eq__(self, other):
...         if self is other:
...             return True
...         if type(self) is not type(other):
...             return False
...         return self.name == other.name
...
...     def __hash__(self):
...         return hash(self.name)
```

이 코드는 이전 [해시 테이블의 동치성 테스트](build-hash-table-prototype-tdd.md#test-equality-hashtables)를 거쳤다면 친숙해 보일 것이다. 간단히 말하면 다른 객체가 `.name` 특성과 동일한 타입의 인스턴스, 다른 타입의 인스턴스 또는 동일한 타입의 다른 인스턴스인지 확인할 수 있다.

> **Notes:** `.__eq__()`와 `__hash__()` 같은 특수 메서드의 코딩은 반복적이고 지루하며 오류가 발생하기 쉽다. Python 3.7 이상을 사용하는 경우 [데이터 클래스(data classes)](https://realpython.com/python-data-classes/)를 사용하여 동일한 효과를 보다 압축적으로 얻을 수 있다.
>
```python
@dataclass(unsafe_hash=True)
class Person:
    name: str
```
>
> 데이터 클래스가 클래스 속성에 `.__eq__()`를 생성하는 동안, 정확한 `.__hash()__` 메서드를 생성할 수 있도록 `unsafe_hash` 옵션을 설정해야 한다. 

`.__eq__()`와 `.__hash__()`를 구현한 후, `Person` 클래스 인스턴스를 딕셔너리의 키로 사용할 수 있다.

```python
>>> alice = Person("Alice")
>>> bob = Person("Bob")

>>> employees = {alice: "project manager", bob: "engineer"}

>>> employees[bob]
'engineer'

>>> employees[Person("Bob")]
'engineer'
```

완벽해! 이전에 작성한 bob 참조를 사용하여 직원을 찾거나 새로운 `Person` 인스턴스("Bob")를 사용해도 문제가 없다. 불행하게도, Bob이 갑자기 이름을 바꾸고 대신 Bobby로 할 것을 결정했을 때 상황은 복잡해진다.

```python
>>> bob.name = "Bobby"

>>> employees[bob]
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    employees[bob]
KeyError: <__main__.Person object at 0x7f607e325e40>

>>> employees[Person("Bobby")]
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    employees[Person("Bobby")]
KeyError: <__main__.Person object at 0x7f607e16ed10>

>>> employees[Person("Bob")]
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    employees[Person("Bob")]
KeyError: <__main__.Person object at 0x7f607e1769e0>
```

이전에 삽입한 원래 키 객체를 사용하더라도 더 이상 해당 값을 검색할 수 없다. 그러나 더 놀라운 것은 업데이트된 사람의 이름이나 이전 이름을 가진 새 키 객체를 통해 값을 액세스할 수 없다는 것이다. 그 이유를 알 수 있을까요?

원래 키의 해시 코드는 연결된 값이 저장된 버킷을 결정한다. 키의 상태를 변환하면 해시 코드가 예상하는 값이 없는 완전히 다른 버킷 또는 슬롯을 나타낸다. 하지만 예전 이름을 키로 사용하는 것도 도움이 되지 않아요. 올바른 버킷을 가리키지만 저장된 키가 변형되어 "Bob"과 "Bobby"의 동일성 비교가 일치하지 않아 False로 평가된다.

따라서 해시가 예상대로 작동하려면 해시 코드가 **불변**이어야 한다. 해시 코드는 일반적으로 객체의 속성에서 얻으므로, 상태는 고정되어야 하며 시간이 지나도 변하지 않아야 한다. 실제로 이것은 해시 테이블 키로 의도된 객체 자체가 불변해야 한다는 것을 의미하기도 한다.

요약하자면, 해시 가능한 데이터 타입은 다음과 같은 특성을 가지고 있다.

1. 인스턴스의 **해시 코드**를 계산하는 `.__hash__()` 메서드가 있다
2. **값**으로 인스턴스를 **비교**하는 `.__eq__()` 메서드가 있다
3. 인스턴스 수명 동안 변경되지 않는 **불변** 해시 코드가 있다
4. **hash-equal** 계약 준수

해시 가능 유형의 네 번째이자 마지막 특성은 **hash-equal** 계약을 준수해야 한다는 것이다. 다음 하위 장에서 자세히 설명한다. 즉, 동일한 값을 갖는 객체는 동일한 해시 코드를 가져야 한다.

## <a name='hash-equal-contract'>Hash-Equal 계약</a>
사용자 정의 클래스를 해시 테이블 키로 사용할 때 문제를 방지하려면 hash-equal 계약을 준수해야 한다. 그 계약에 대해 한 가지 기억해야 할 것이 있다면, 그것은 `.__eq__()`를 구현할 때 항상 해당하는 `.__hash__()`를 구현해야 한다. 두 가지 방법을 모두 구현할 필요가 없는 유일한 경우는 **데이터 클래스** 또는 이미 이 작업을 수행한 불변 **명명된 튜플(named tuple)**과 같은 래퍼를 사용할 때 뿐이다.

또한 데이터 타입의 객체를 딕셔너리의 키 또는 집합 원소로 사용하지 않을 것이 확실하다면 두 가지 방법을 모두 구현하지 않는 것도 괜찮을 수 있다. 하지만 그렇게 확신할 수 있을까요?

> **Notes**: 데이터 클래스 또는 명명된 튜플을 사용할 수 없으며 클래스에서 두 개 이상의 필드를 수동으로 비교하고 해시하려면 해당 필드를 튜플로 래핑하세오.
>
```python
class Person:
    def __init__(self, name, date_of_birth, married):
        self.name = name
        self.date_of_birth = date_of_birth
        self.married = married

    def __hash__(self):
        return hash(self._fields)

    def __eq__(self, other):
        if self is other:
            return True
        if type(self) is not type(other):
            return False
        return self._fields == other._fields

    @property
    def _fields(self):
        return self.name, self.date_of_birth, self.married
```
>
> 클래스에 상대적으로 많은 필드가 있는 경우 해당 튜플을 반환하여 프라이트 속성(private property)을 정의하는 것이 바람직하다.

두 가지 방법을 모두 원하는 대로 구현할 수 있지만, 두 개의 동일한 객체가 해시 코드를 동일하게 하려면 해시가 되어야 한다는 hash-equal 계약을 충족해야 한다. 이렇게 하면 제공된 키를 기반으로 정확히 버킷을 찾을 수 있다. 그러나 충돌로 인해 동일한 해시 코드가 동일하지 않은 값을 공유할 수 있기 때문이다. 그러나 그 반대는 사실이 아니다. 다음 두 가지 의미를 사용하여 이를 더 공식적으로 표현할 수 있다.

1. `a = b ⇒ hash(a) = hash(b)`
2. `hash(a) = hash(b) ⇏ a = b`

hash-equal 계약은 일방적 계약이다. 논리적으로 두 키가 같으면 해시 코드도 같아야 한다. 반면에 두 키가 동일한 해시 코드를 공유하는 경우 동일한 키일 가능성이 있지만 서로 다른 키일 가능성도 있다. 그들이 일치하는지 확인하기 위해 그들을 비교할 필요가 있다. [대립(contraposition)](https://en.wikipedia.org/wiki/Contraposition) 법칙에 의해, 첫 번째 특성으로부터 다른 의미를 도출할 수 있다.

`hash(a) ≠ hash(b) ⇒ a ≠ b`

두 개의 키가 다른 해시 코드를 가지고 있다는 것을 알고 있다면, 그들을 비교하는 것은 의미가 없다. 동일성 검정은 비용이 많이 들기 때문에 성능 향상에 도움이 될 수 있다.

일부 IDE에서는 `__hash__()`와 `.__eq__()` 메서드를 자동으로 생성할 수 있지만, 클래스 속성을 변경할 때마다 메서드를 다시 생성해야 한다. 따라서 해시 가능한 유형의 적절한 구현을 보장하기 위해 가능할 때마다 Python의 데이터 클래스 또는 명명된 튜플을 사용하세요.
